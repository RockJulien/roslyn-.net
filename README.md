# README #

Simple console application to show the use of Roslyn and some helpers to create new compilation units from scratch with comments.

### What is this repository for? ###

* Some helpers to use the .Net Roslyn APIs
* 1.0

### How do I get set up? ###

* Compile and Run

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* People that would like to pre or post process their compiled source code.
* People that would like to auto generate code from other code part to automate redundant behaviors and classes.