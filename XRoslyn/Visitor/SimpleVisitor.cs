﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using XRoslyn.Extensions;

namespace XRoslyn.Visitor
{
    /// <summary>
    /// Definition of the <see cref="SimpleVisitor"/> class.
    /// </summary>
    public class SimpleVisitor : CSharpSyntaxWalker
    {
        #region Fields

        /// <summary>
        /// Stores the parsed code tree.
        /// </summary>
        private SyntaxTree mParsedTree;

        /// <summary>
        /// Stores the indentation count.
        /// </summary>
        private static int Tabs = 0;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the parsed code tree.
        /// </summary>
        public SyntaxTree ParsedTree
        {
            get
            {
                return this.mParsedTree;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleVisitor"/> class.
        /// </summary>
        /// <param name="pToParse">The code to visit.</param>
        public SimpleVisitor(string pToParse) :
        base( SyntaxWalkerDepth.StructuredTrivia ) // Indicates it must visit everything (default SyntaxWalkerDepth.Node means only visit top level declarations)
        {
            this.mParsedTree = CSharpSyntaxTree.ParseText( pToParse );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Returns the original visitor string back.
        /// </summary>
        /// <returns></returns>
        public string GetString()
        {
            return this.mParsedTree.GetString();
        }

        /// <summary>
        /// Visits the supplied parsed code.
        /// </summary>
        public void Visit()
        {
            this.Visit( this.mParsedTree.GetRoot() );
        }

        /// <summary>
        /// Visit a syntax node.
        /// </summary>
        /// <param name="node"></param>
        public override void Visit(SyntaxNode node)
        {
            Tabs++;

            string lIndents = new string( ' ', Tabs * 3 );
            Console.WriteLine( lIndents + node.Kind() );
            base.Visit( node );

            Tabs--;
        }

        /// <summary>
        /// Visit a syntax token
        /// </summary>
        /// <param name="token"></param>
        public override void VisitToken(SyntaxToken token)
        {
            string lIndents = new string( ' ', Tabs * 3 );
            Console.WriteLine( string.Format( "{0}{1}:\t{2}", lIndents, token.Kind(), token ) );
            base.VisitToken( token );
        }

        /// <summary>
        /// Visit a let clause node.
        /// </summary>
        /// <param name="node"></param>
        public override void VisitLetClause(LetClauseSyntax node)
        {
            Console.WriteLine( string.Format( "Found a let clause {0}", node.Identifier.Text ) );
            base.VisitLetClause( node );
        }

        #endregion Methods
    }
}
