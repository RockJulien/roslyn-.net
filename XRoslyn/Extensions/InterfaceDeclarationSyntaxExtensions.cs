﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using System.Collections.Generic;
using XRoslyn.Helpers;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="InterfaceDeclarationSyntaxExtensions"/> extensions class.
    /// </summary>
    public static class InterfaceDeclarationSyntaxExtensions
    {
        #region Methods

        /// <summary>
        /// Adds an auto property to an interface (property with get/set having empty bodies)
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static InterfaceDeclarationSyntax AddAutoProperty(this InterfaceDeclarationSyntax pThis, string pName, string pType, SyntaxGenerator pGenerator)
        {
            PropertyDeclarationSyntax lNewProperty = (pGenerator.PropertyDeclaration( pName, // property name
                                                                                      SyntaxFactory.ParseTypeName( pType ), // Return type
                                                                                      Accessibility.NotApplicable ) as PropertyDeclarationSyntax).ToAutoImplementedProperty( pGenerator );

            return pThis.AddMembers( lNewProperty.AddComment( "" ) );
        }

        /// <summary>
        /// Adds a method to an interface
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pModifier"></param>
        /// <param name="pReturnType"></param>
        /// <param name="pMethodName"></param>
        /// <param name="pParams"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static InterfaceDeclarationSyntax AddMethod(this InterfaceDeclarationSyntax pThis, DeclarationModifiers pModifier, string pReturnType, string pMethodName, IEnumerable<Parameter> pParams, SyntaxGenerator pGenerator)
        {
            MethodDeclarationSyntax lNewMethod = pGenerator.MethodDeclaration( pMethodName, // method name
                                                                               SyntaxNodeHelpers.CreateParameterNodes( pParams, pGenerator ),
                                                                               null,
                                                                               SyntaxFactory.ParseTypeName( pReturnType ),
                                                                               Accessibility.NotApplicable,
                                                                               pModifier ) as MethodDeclarationSyntax;

            return pThis.AddMembers( lNewMethod.AddComment( "Method in charge of " + pMethodName, pParams ) );
        }

        #endregion Methods
    }
}
