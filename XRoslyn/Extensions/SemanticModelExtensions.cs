﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="SemanticModelExtensions"/> class.
    /// </summary>
    public static class SemanticModelExtensions
    {
        #region Methods

        /// <summary>
        /// Retrieves the base classes of the given type.
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pType"></param>
        /// <returns></returns>
        public static IEnumerable<INamedTypeSymbol> GetBaseClasses(this SemanticModel pThis, BaseTypeDeclarationSyntax pType)
        {
            ITypeSymbol lClassSymbol = pThis.GetDeclaredSymbol( pType ) as ITypeSymbol;
            List<INamedTypeSymbol> lReturnValues = new List<INamedTypeSymbol>();
            while ( lClassSymbol.BaseType != null )
            {
                lReturnValues.Add(lClassSymbol.BaseType);
                if ( lClassSymbol.AllInterfaces != null )
                {
                    lReturnValues.AddRange( lClassSymbol.AllInterfaces );
                }

                lClassSymbol = lClassSymbol.BaseType;
            }

            return lReturnValues;
        }

        #endregion Methods
    }
}
