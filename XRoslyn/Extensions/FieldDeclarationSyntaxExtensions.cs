﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using XRoslyn.Helpers;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="FieldDeclarationSyntaxExtensions"/> extensions class.
    /// </summary>
    public static class FieldDeclarationSyntaxExtensions
    {
        #region Methods

        /// <summary>
        /// Adds a comment to a field.
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pComment"></param>
        /// <returns></returns>
        public static FieldDeclarationSyntax AddComment(this FieldDeclarationSyntax pThis, string pComment)
        {
            string lFieldName = pThis.Declaration.Variables[ 0 ].Identifier.Text;
            
            SyntaxTriviaList lLeadingTrivia = pThis.GetLeadingTrivia().Insert( 0,
                            SyntaxFactory.Trivia(
                                    SyntaxFactory.DocumentationCommentTrivia( SyntaxKind.SingleLineDocumentationCommentTrivia,
                                        SyntaxFactory.List( new XmlNodeSyntax[]
                                        {
                                                SyntaxNodeHelpers.CreateFieldSummary( lFieldName, pComment ),
                                                SyntaxNodeHelpers.NewXmlLine()
                                        }))));

            return pThis.WithLeadingTrivia( lLeadingTrivia );
        }

        #endregion Methods
    }
}
