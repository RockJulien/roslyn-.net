﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using System.Collections.Generic;
using XRoslyn.Helpers;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="ClassDeclarationSyntaxExtensions"/> extensions class.
    /// </summary>
    public static class ClassDeclarationSyntaxExtensions
    {
        #region Methods

        /// <summary>
        /// Adds a field to a class
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static ClassDeclarationSyntax AddField(this ClassDeclarationSyntax pThis, Accessibility pAccessibility, string pName, string pType, SyntaxGenerator pGenerator)
        {
            FieldDeclarationSyntax lNewField = pGenerator.FieldDeclaration( "m" + pName,
                                                                            SyntaxFactory.ParseTypeName( pType ), // field type
                                                                            pAccessibility ) as FieldDeclarationSyntax; // Accessor private

            return pThis.AddMembers( lNewField.AddComment( "" ) );
        }

        /// <summary>
        /// Adds an auto property to a class (property with get/set having empty bodies)
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static ClassDeclarationSyntax AddAutoProperty(this ClassDeclarationSyntax pThis, Accessibility pAccessibility, string pName, string pType, SyntaxGenerator pGenerator)
        {
            PropertyDeclarationSyntax lNewProperty = (pGenerator.PropertyDeclaration( pName, // property name
                                                                                      SyntaxFactory.ParseTypeName( pType ), // Return type
                                                                                      pAccessibility ) as PropertyDeclarationSyntax).ToAutoImplementedProperty( pGenerator );

            return pThis.AddMembers( lNewProperty.AddComment( "" ) );
        }

        /// <summary>
        /// Adds a full property to a class (property with get/set having bodies using backfields)
        /// 
        /// NOTE: Backfields with follow the naming rule => m + PropertyName
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static ClassDeclarationSyntax AddFullProperty(this ClassDeclarationSyntax pThis, Accessibility pAccessibility, string pName, string pType, SyntaxGenerator pGenerator)
        {
            PropertyDeclarationSyntax lNewProperty = (pGenerator.PropertyDeclaration( pName, // property name
                                                                                      SyntaxFactory.ParseTypeName( pType ), // Return type
                                                                                      pAccessibility ) as PropertyDeclarationSyntax).ToFullProperty( pGenerator );

            return pThis.AddMembers( lNewProperty.AddComment( "" ) );
        }

        /// <summary>
        /// Adds a default constructor to a class
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static ClassDeclarationSyntax AddDefaultConstructor(this ClassDeclarationSyntax pThis, Accessibility pAccessibility, SyntaxGenerator pGenerator)
        {
            string lClassName = pThis.Identifier.Text;
            ConstructorDeclarationSyntax lNewConstructor = pGenerator.ConstructorDeclaration( lClassName, // class name
                                                                                              null,
                                                                                              pAccessibility ) as ConstructorDeclarationSyntax; // Accessor
            
            return pThis.AddMembers( lNewConstructor.AddComment( "" ) );
        }

        /// <summary>
        /// Adds a constructor to a class
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pParams"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static ClassDeclarationSyntax AddConstructor(this ClassDeclarationSyntax pThis, Accessibility pAccessibility, IEnumerable<Parameter> pParams, SyntaxGenerator pGenerator)
        {
            string lClassName = pThis.Identifier.Text;
            ConstructorDeclarationSyntax lNewConstructor = pGenerator.ConstructorDeclaration( lClassName, // class name
                                                                                              SyntaxNodeHelpers.CreateParameterNodes( pParams, pGenerator ),
                                                                                              pAccessibility,  // Accessor 
                                                                                              default(DeclarationModifiers),
                                                                                              null, // base constructor call arguments (e.g: base( pXX, pYY ) )
                                                                                              null ) as ConstructorDeclarationSyntax; // Members assignments in ctor (e.g: mXX = pXX;)

            return pThis.AddMembers( lNewConstructor.AddComment( "", pParams ) );
        }

        /// <summary>
        /// Adds a method to a class
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pModifier"></param>
        /// <param name="pReturnType"></param>
        /// <param name="pMethodName"></param>
        /// <param name="pParams"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static ClassDeclarationSyntax AddMethod(this ClassDeclarationSyntax pThis, Accessibility pAccessibility, DeclarationModifiers pModifier, string pReturnType, string pMethodName, IEnumerable<Parameter> pParams, SyntaxGenerator pGenerator)
        {
            MethodDeclarationSyntax lNewMethod = pGenerator.MethodDeclaration( pMethodName, // method name
                                                                               SyntaxNodeHelpers.CreateParameterNodes( pParams, pGenerator ),
                                                                               null,
                                                                               SyntaxFactory.ParseTypeName( pReturnType ),
                                                                               pAccessibility,
                                                                               pModifier ) as MethodDeclarationSyntax;

            return pThis.AddMembers( lNewMethod.AddComment( "Method in charge of " + pMethodName, pParams ) );
        }
        
        #endregion Methods
    }
}
