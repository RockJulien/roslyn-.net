﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using XRoslyn.Helpers;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="ConstructorDeclarationSyntaxExtensions"/> extensions class.
    /// </summary>
    public static class ConstructorDeclarationSyntaxExtensions
    {
        #region Methods

        /// <summary>
        /// Adds a comment to a constructor
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pComment"></param>
        /// <param name="pParams">The ctor params if any</param>
        /// <returns></returns>
        public static ConstructorDeclarationSyntax AddComment(this ConstructorDeclarationSyntax pThis, string pComment, IEnumerable<Parameter> pParams = null)
        {
            string lConstructorName = pThis.Identifier.Text;

            List<XmlNodeSyntax> lXmlNodes = new List<XmlNodeSyntax>()
            {
                SyntaxNodeHelpers.CreateConstructorSummary( lConstructorName, pComment ),
                SyntaxNodeHelpers.NewXmlLine()
            };

            if ( pParams != null )
            {
                foreach ( Parameter lParam in pParams )
                {
                    lXmlNodes.AddRange( new XmlNodeSyntax[] 
                    {
                         SyntaxNodeHelpers.CreateParamComment( lParam.Name, "The " + lParam.Name ),
                         SyntaxNodeHelpers.NewXmlLine()
                    });
                }
            }

            SyntaxTriviaList lLeadingTrivia = pThis.GetLeadingTrivia().Insert( 0,
                                SyntaxFactory.Trivia(
                                    SyntaxFactory.DocumentationCommentTrivia( SyntaxKind.SingleLineDocumentationCommentTrivia,
                                        SyntaxFactory.List( lXmlNodes ))));

            return pThis.WithLeadingTrivia( lLeadingTrivia );
        }

        #endregion Methods
    }
}
