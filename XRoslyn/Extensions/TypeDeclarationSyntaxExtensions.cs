﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using System.Collections.Generic;
using System.Linq;
using XRoslyn.Helpers;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="TypeDeclarationSyntaxExtensions"/> extensions class.
    /// </summary>
    public static class TypeDeclarationSyntaxExtensions
    {
        #region Methods

        /// <summary>
        /// Gets the type syntax class name.
        /// </summary>
        /// <param name="pTypeSyntax"></param>
        /// <returns>The type class name.</returns>
        public static string Name(this BaseTypeDeclarationSyntax pTypeSyntax)
        {
            return pTypeSyntax.Identifier.ToFullString();
        }

        /// <summary>
        /// Retrieves the type symbol corresponding to the type syntax node in the compilation.
        /// </summary>
        /// <param name="pTypeSyntax"></param>
        /// <param name="pCompilation"></param>
        /// <returns></returns>
        public static ITypeSymbol ToTypeSymbol(this BaseTypeDeclarationSyntax pTypeSyntax, Compilation pCompilation)
        {
            SemanticModel lModel = pCompilation.GetSemanticModel( pTypeSyntax.SyntaxTree );
            return lModel.GetDeclaredSymbol( pTypeSyntax ) as ITypeSymbol;
        }

        /// <summary>
        /// Retrieves the type syntax node corresponding to the given type symbol from the compilation.
        /// </summary>
        /// <param name="pSymbol"></param>
        /// <param name="pCompilation"></param>
        /// <returns></returns>
        public static BaseTypeDeclarationSyntax ToTypeSyntax(this ITypeSymbol pSymbol, Compilation pCompilation)
        {
            foreach (SyntaxReference lTreeRef in pSymbol.DeclaringSyntaxReferences )
            {
                SemanticModel lModel = pCompilation.GetSemanticModel( lTreeRef.SyntaxTree );
                BaseTypeDeclarationSyntax lTypeSyntax = lTreeRef.SyntaxTree.GetRoot().DescendantNodes().OfType<BaseTypeDeclarationSyntax>().FirstOrDefault( pElt => lModel.GetDeclaredSymbol( pElt ) == pSymbol );
                if( lTypeSyntax != null )
                {
                    return lTypeSyntax;
                }
            }

            return null;
        }

        /// <summary>
        /// Adds a comment to a class
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pComment"></param>
        /// <returns></returns>
        public static BaseTypeDeclarationSyntax AddComment(this BaseTypeDeclarationSyntax pThis, string pComment)
        {
            string lClassName = pThis.Identifier.Text;

            SyntaxTriviaList lLeadingClassTrivia = pThis.GetLeadingTrivia().Insert( 0, 
                        SyntaxFactory.Trivia(
                                SyntaxFactory.DocumentationCommentTrivia(SyntaxKind.SingleLineDocumentationCommentTrivia,
                                    SyntaxFactory.List(new XmlNodeSyntax[]
                                    {
                                            SyntaxNodeHelpers.CreateClassSummary( lClassName, pComment ),
                                            SyntaxNodeHelpers.NewXmlLine()
                                    }))));

            return pThis.WithLeadingTrivia( lLeadingClassTrivia );
        }

        /// <summary>
        /// Adds a field to a type
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static BaseTypeDeclarationSyntax AddField(this BaseTypeDeclarationSyntax pThis, Accessibility pAccessibility, string pName, string pType, SyntaxGenerator pGenerator)
        {
            if ( pThis is StructDeclarationSyntax )
            {
                return (pThis as StructDeclarationSyntax).AddField( pAccessibility, pName, pType, pGenerator );
            }
            else if ( pThis is ClassDeclarationSyntax )
            {
                return (pThis as ClassDeclarationSyntax).AddField( pAccessibility, pName, pType, pGenerator );
            }
            else if ( pThis is EnumDeclarationSyntax )
            {
                return (pThis as EnumDeclarationSyntax).AddField( pName, pGenerator );
            }

            return pThis;
        }

        /// <summary>
        /// Adds an auto property to a type (property with get/set having empty bodies)
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static BaseTypeDeclarationSyntax AddAutoProperty(this BaseTypeDeclarationSyntax pThis, Accessibility pAccessibility, string pName, string pType, SyntaxGenerator pGenerator)
        {
            if ( pThis is StructDeclarationSyntax )
            {
                return (pThis as StructDeclarationSyntax).AddAutoProperty( pAccessibility, pName, pType, pGenerator );
            }
            else if ( pThis is ClassDeclarationSyntax )
            {
                return (pThis as ClassDeclarationSyntax).AddAutoProperty( pAccessibility, pName, pType, pGenerator );
            }
            else if ( pThis is InterfaceDeclarationSyntax )
            {
                return (pThis as InterfaceDeclarationSyntax).AddAutoProperty( pName, pType, pGenerator );
            }

            return pThis;
        }

        /// <summary>
        /// Adds a full property to a type (property with get/set having bodies using backfields)
        /// 
        /// NOTE: Backfields with follow the naming rule => m + PropertyName
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static BaseTypeDeclarationSyntax AddFullProperty(this BaseTypeDeclarationSyntax pThis, Accessibility pAccessibility, string pName, string pType, SyntaxGenerator pGenerator)
        {
            if ( pThis is StructDeclarationSyntax )
            {
                return (pThis as StructDeclarationSyntax).AddFullProperty( pAccessibility, pName, pType, pGenerator );
            }
            else if ( pThis is ClassDeclarationSyntax )
            {
                return (pThis as ClassDeclarationSyntax).AddFullProperty( pAccessibility, pName, pType, pGenerator );
            }

            return pThis;
        }

        /// <summary>
        /// Adds a default constructor to a type
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static BaseTypeDeclarationSyntax AddDefaultConstructor(this BaseTypeDeclarationSyntax pThis, Accessibility pAccessibility, SyntaxGenerator pGenerator)
        {
            if ( pThis is StructDeclarationSyntax )
            {
                return (pThis as StructDeclarationSyntax).AddDefaultConstructor( pAccessibility, pGenerator );
            }
            else if ( pThis is ClassDeclarationSyntax )
            {
                return (pThis as ClassDeclarationSyntax).AddDefaultConstructor( pAccessibility, pGenerator );
            }
            
            return pThis;
        }

        /// <summary>
        /// Adds a constructor to a type
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pParams"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static BaseTypeDeclarationSyntax AddConstructor(this BaseTypeDeclarationSyntax pThis, Accessibility pAccessibility, IEnumerable<Parameter> pParams, SyntaxGenerator pGenerator)
        {
            if ( pThis is StructDeclarationSyntax )
            {
                return (pThis as StructDeclarationSyntax).AddConstructor( pAccessibility, pParams, pGenerator );
            }
            else if ( pThis is ClassDeclarationSyntax )
            {
                return (pThis as ClassDeclarationSyntax).AddConstructor( pAccessibility, pParams, pGenerator );
            }
            
            return pThis;
        }

        /// <summary>
        /// Adds a method to a type
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pAccessibility"></param>
        /// <param name="pModifier"></param>
        /// <param name="pReturnType"></param>
        /// <param name="pMethodName"></param>
        /// <param name="pParams"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static BaseTypeDeclarationSyntax AddMethod(this BaseTypeDeclarationSyntax pThis, Accessibility pAccessibility, DeclarationModifiers pModifier, string pReturnType, string pMethodName, IEnumerable<Parameter> pParams, SyntaxGenerator pGenerator)
        {
            if ( pThis is StructDeclarationSyntax )
            {
                return (pThis as StructDeclarationSyntax).AddMethod( pAccessibility, pModifier, pReturnType, pMethodName, pParams, pGenerator );
            }
            else if ( pThis is ClassDeclarationSyntax )
            {
                return (pThis as ClassDeclarationSyntax).AddMethod( pAccessibility, pModifier, pReturnType, pMethodName, pParams, pGenerator );
            }
            else if ( pThis is InterfaceDeclarationSyntax )
            {
                return (pThis as InterfaceDeclarationSyntax).AddMethod( pModifier, pReturnType, pMethodName, pParams, pGenerator );
            }
            
            return pThis;
        }

        #endregion Methods
    }
}
