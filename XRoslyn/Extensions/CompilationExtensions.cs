﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="CompilationExtensions"/> class.
    /// </summary>
    public static class CompilationExtensions
    {
        #region Methods

        /// <summary>
        /// Checks whether there are compilation errors or not (and print them to console if any)
        /// </summary>
        /// <param name="pThis"></param>
        /// <returns>True if compilation successful with no errors, false otherwise.</returns>
        public static bool ReviewDiagnosticMessages(this Compilation pThis)
        {
            ImmutableArray<Diagnostic> lResults = pThis.GetDiagnostics();
            bool lResult = true;
            foreach ( Diagnostic lDiagnostic in lResults.Where( pDiagnostic => pDiagnostic.Severity == DiagnosticSeverity.Error ) )
            {
                lResult = false;
                Console.WriteLine( lDiagnostic.GetMessage() + lDiagnostic.Location );
            }

            return lResult;
        }

        /// <summary>
        /// Finds the derived classes or another base classe or an interface.
        /// </summary>
        /// <param name="pThis">The compilation object owning SyntaxTree(s), that is, classes source code</param>
        /// <param name="pTarget">The type to look for.</param>
        /// <returns></returns>
        public static IEnumerable<BaseTypeDeclarationSyntax> FindDerivedInterfaces(this Compilation pThis, INamedTypeSymbol pTarget)
        {
            foreach (SyntaxTree lTree in pThis.SyntaxTrees)
            {
                SemanticModel lSemanticModel = pThis.GetSemanticModel( lTree );
                foreach (TypeDeclarationSyntax lType in lTree.GetRoot().DescendantNodes().OfType<TypeDeclarationSyntax>())
                {
                    if ( lType is InterfaceDeclarationSyntax )
                    {
                        ITypeSymbol lClassSymbol = lSemanticModel.GetDeclaredSymbol( lType ) as ITypeSymbol;
                        if ( lClassSymbol.AllInterfaces.Contains( pTarget ) )
                        {
                            yield return lType;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Finds the derived classes or interfaces.
        /// </summary>
        /// <param name="pThis">The compilation object owning SyntaxTree(s), that is, classes source code</param>
        /// <param name="pTarget">The type to look for.</param>
        /// <returns></returns>
        public static IEnumerable<BaseTypeDeclarationSyntax> FindDerivedClasses(this Compilation pThis, INamedTypeSymbol pTarget)
        {
            foreach (SyntaxTree lTree in pThis.SyntaxTrees)
            {
                SemanticModel lSemanticModel = pThis.GetSemanticModel(lTree);
                foreach (TypeDeclarationSyntax lType in lTree.GetRoot().DescendantNodes().OfType<TypeDeclarationSyntax>())
                {
                    IEnumerable<INamedTypeSymbol> lBaseClasses = SemanticModelExtensions.GetBaseClasses(lSemanticModel, lType);
                    if (lBaseClasses != null)
                    {
                        if (lBaseClasses.Contains(pTarget))
                        {
                            yield return lType;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Finds the derived classes or another base classe or an interface.
        /// </summary>
        /// <param name="pThis">The compilation object owning SyntaxTree(s), that is, classes source code</param>
        /// <param name="pTarget">The type to look for.</param>
        /// <returns></returns>
        public static IEnumerable<BaseTypeDeclarationSyntax> FindAllDerivedByType(this Compilation pThis, INamedTypeSymbol pTarget)
        {
            foreach (SyntaxTree lTree in pThis.SyntaxTrees)
            {
                SemanticModel lSemanticModel = pThis.GetSemanticModel(lTree);
                foreach (TypeDeclarationSyntax lType in lTree.GetRoot().DescendantNodes().OfType<TypeDeclarationSyntax>())
                {
                    if ( lType is InterfaceDeclarationSyntax )
                    {
                        ITypeSymbol lClassSymbol = lSemanticModel.GetDeclaredSymbol( lType ) as ITypeSymbol;
                        if ( lClassSymbol.AllInterfaces.Contains( pTarget ) )
                        {
                            yield return lType;
                        }
                    }
                    else
                    {
                        IEnumerable<INamedTypeSymbol> lBaseClasses = SemanticModelExtensions.GetBaseClasses(lSemanticModel, lType);
                        if (lBaseClasses != null)
                        {
                            if (lBaseClasses.Contains(pTarget))
                            {
                                yield return lType;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Reviews the symbol table in the compiled sources.
        /// </summary>
        /// <param name="pThis"></param>
        public static void ReviewSymbolTable(this Compilation pThis)
        {
            foreach ( INamespaceSymbol lGlobalNamespace in pThis.Assembly.GlobalNamespace.GetNamespaceMembers().Where( pMember => pMember.CanBeReferencedByName ) )
            {
                ReviewSymbolTable( lGlobalNamespace );
            }
        }

        /// <summary>
        /// Reviews the symbol table in the member.
        /// </summary>
        /// <param name="pMember"></param>
        private static void ReviewSymbolTable(INamespaceSymbol pMember)
        {
            Console.WriteLine( pMember.Name );
            foreach ( INamedTypeSymbol lType in pMember.GetTypeMembers().Where( pType => pType.CanBeReferencedByName ) )
            {
                Console.WriteLine("\t{0}:{1}", lType.TypeKind, lType.Name);
                foreach ( ISymbol lSubMember in lType.GetMembers().Where( pSubMember => pSubMember.CanBeReferencedByName ) )
                {
                    Console.WriteLine( "\t\t{0}:{1}", lSubMember.Kind, lSubMember.Name );
                }
            }

            // Look for sub namespaces and recurse.
            foreach ( INamespaceSymbol lSubNamespace in pMember.GetNamespaceMembers().Where( pSubNamespace => pSubNamespace.CanBeReferencedByName ) )
            {
                ReviewSymbolTable( lSubNamespace );
            }
        }

        #endregion Methods
    }
}
