﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using XRoslyn.Helpers;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="EnumMemberDeclarationSyntaxExtensions"/> extensions class.
    /// </summary>
    public static class EnumMemberDeclarationSyntaxExtensions
    {
        #region Methods

        /// <summary>
        /// Adds a comment to a field.
        /// </summary>
        /// <param name="pThis"></param>
        /// <returns></returns>
        public static EnumMemberDeclarationSyntax AddComment(this EnumMemberDeclarationSyntax pThis)
        {
            string lFieldName = pThis.Identifier.Text;
            
            SyntaxTriviaList lLeadingTrivia = pThis.GetLeadingTrivia().Insert( 0,
                            SyntaxFactory.Trivia(
                                    SyntaxFactory.DocumentationCommentTrivia( SyntaxKind.SingleLineDocumentationCommentTrivia,
                                        SyntaxFactory.List( new XmlNodeSyntax[]
                                        {
                                                SyntaxNodeHelpers.CreateEnumValueSummary( lFieldName ),
                                                SyntaxNodeHelpers.NewXmlLine()
                                        }))));

            return pThis.WithLeadingTrivia( lLeadingTrivia );
        }

        #endregion Methods
    }
}
