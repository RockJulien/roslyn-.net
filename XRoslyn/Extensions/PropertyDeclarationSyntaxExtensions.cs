﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using System.Linq;
using XRoslyn.Helpers;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="PropertyDeclarationSyntaxExtensions"/> class.
    /// </summary>
    public static class PropertyDeclarationSyntaxExtensions
    {
        #region Methods

        /// <summary>
        /// Adds a comment to a field.
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pComment"></param>
        /// <returns></returns>
        public static PropertyDeclarationSyntax AddComment(this PropertyDeclarationSyntax pThis, string pComment)
        {
            string lPropertyName = pThis.Identifier.Text;
            
            SyntaxList<AccessorDeclarationSyntax> lAccessors = pThis.AccessorList.Accessors;

            AccessorDeclarationSyntax lGetterDeclaration = lAccessors.FirstOrDefault( pElt => pElt.Kind() == SyntaxKind.GetAccessorDeclaration );
            AccessorDeclarationSyntax lSetterDeclaration = lAccessors.FirstOrDefault( pElt => pElt.Kind() == SyntaxKind.SetAccessorDeclaration );

            SyntaxTriviaList lLeadingTrivia = pThis.GetLeadingTrivia().Insert( 0,
                            SyntaxFactory.Trivia(
                                    SyntaxFactory.DocumentationCommentTrivia( SyntaxKind.SingleLineDocumentationCommentTrivia,
                                        SyntaxFactory.List( new XmlNodeSyntax[]
                                        {
                                                SyntaxNodeHelpers.CreatePropertySummary( lPropertyName, lGetterDeclaration != null, lSetterDeclaration != null, pComment ),
                                                SyntaxNodeHelpers.NewXmlLine()
                                        }))));

            return pThis.WithLeadingTrivia( lLeadingTrivia );
        }

        /// <summary>
        /// Checks whether a property declaration is an auto implemented one or not, that is, does not
        /// have getter and setter body or does.
        /// </summary>
        /// <param name="pPropertyDeclaration"></param>
        /// <returns></returns>
        public static bool IsAutoImplementedProperty(this PropertyDeclarationSyntax pPropertyDeclaration)
        {
            SyntaxList<AccessorDeclarationSyntax> lAccessors = pPropertyDeclaration.AccessorList.Accessors;

            AccessorDeclarationSyntax lGetterDeclaration = lAccessors.FirstOrDefault( pElt => pElt.Kind() == SyntaxKind.GetAccessorDeclaration );
            AccessorDeclarationSyntax lSetterDeclaration = lAccessors.FirstOrDefault( pElt => pElt.Kind() == SyntaxKind.SetAccessorDeclaration );
            if ( lGetterDeclaration == null || 
                 lSetterDeclaration == null )
            {
                return false;
            }

            // Is Auto Implemented if no bodies
            return lGetterDeclaration.Body == null && 
                   lSetterDeclaration.Body == null;
        }

        /// <summary>
        /// Turns the property declartion into an auto implemented property, that is, with empty getter and setter bodies.
        /// </summary>
        /// <param name="pPropertyDeclaration"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static PropertyDeclarationSyntax ToAutoImplementedProperty(this PropertyDeclarationSyntax pPropertyDeclaration, SyntaxGenerator pGenerator)
        {
            return pPropertyDeclaration.WithAccessorList( SyntaxFactory.AccessorList( SyntaxFactory.List( new[] 
            {
                pGenerator.GetAccessorDeclaration(),
                pGenerator.SetAccessorDeclaration()

                // Second way of doing the same (without SyntaxGenerator)
                //SyntaxFactory.AccessorDeclaration( SyntaxKind.GetAccessorDeclaration ).WithSemicolonToken( SyntaxFactory.Token( SyntaxKind.SemicolonToken ) ),
                //SyntaxFactory.AccessorDeclaration( SyntaxKind.SetAccessorDeclaration ).WithSemicolonToken( SyntaxFactory.Token( SyntaxKind.SemicolonToken ) )
            }) ) );
        }

        /// <summary>
        /// Turns the property declartion into an auto implemented property, that is, with empty getter and setter bodies.
        /// </summary>
        /// <param name="pPropertyDeclaration"></param>
        /// <param name="pGenerator"></param>
        /// <param name="pBackingFieldName"></param>
        /// <returns></returns>
        public static PropertyDeclarationSyntax ToFullProperty(this PropertyDeclarationSyntax pPropertyDeclaration, SyntaxGenerator pGenerator, string pBackingFieldName = null)
        {
            string lPropertyName = pPropertyDeclaration.Identifier.Text;
            string lBackingFieldName = string.IsNullOrEmpty( pBackingFieldName ) ? "this.m" + lPropertyName : pBackingFieldName;
            return pPropertyDeclaration.WithAccessorList( SyntaxFactory.AccessorList( SyntaxFactory.List( new[] 
            {
                pGenerator.GetAccessorDeclaration( Accessibility.NotApplicable, new SyntaxNode[] { pGenerator.ReturnStatement( pGenerator.IdentifierName( lBackingFieldName ) ) } ), // get { return mXXX; }
                pGenerator.SetAccessorDeclaration( Accessibility.NotApplicable, new SyntaxNode[] { pGenerator.AssignmentStatement( pGenerator.IdentifierName( lBackingFieldName ), pGenerator.IdentifierName( "value" ) ) } ) // set { mXXX = value; }
            } ) ) );
        }

        #endregion Methods
    }
}
