﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Linq;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="SyntaxTreeExtensions"/> extensions class.
    /// </summary>
    public static class SyntaxTreeExtensions
    {
        #region Methods

        /// <summary>
        /// Gets the class from its syntax tree.
        /// </summary>
        /// <param name="pThis">The syntax tree to get the class of.</param>
        /// <returns>The class declaration.</returns>
        public static ClassDeclarationSyntax Class(this SyntaxTree pThis)
        {
            return pThis.GetRoot().DescendantNodes().FirstOrDefault( pElt => pElt is ClassDeclarationSyntax ) as ClassDeclarationSyntax;
        }

        /// <summary>
        /// Gets the string representation of the given syntax tree.
        /// </summary>
        /// <param name="pThis"></param>
        /// <returns></returns>
        public static string GetString(this SyntaxTree pThis)
        {
            return pThis.GetRoot().ToFullString();
        }

        #endregion Methods
    }
}
