﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using System;
using System.Linq;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="MSBuildWorkspaceExtensions"/> extensions class.
    /// </summary>
    internal static class MSBuildWorkspaceExtensions
    {
        #region Methods

        /// <summary>
        /// Reviews the workspace errors if any.
        /// </summary>
        /// <param name="pThis"></param>
        /// <returns>True if any errors, false otherwise.</returns>
        internal static bool ReviewWorkspaceError(this MSBuildWorkspace pThis)
        {
            bool lResult = false;
            foreach ( WorkspaceDiagnostic lError in pThis.Diagnostics.Where( pElt => pElt.Kind == WorkspaceDiagnosticKind.Failure ) )
            {
                lResult = true;
                Console.WriteLine( string.Format( "-- ERROR: {0}", lError.Message ) );
                Console.WriteLine();
            }

            return lResult;
        }

        #endregion Methods
    }
}
