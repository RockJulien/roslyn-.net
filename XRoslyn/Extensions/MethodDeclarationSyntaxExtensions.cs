﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using XRoslyn.Helpers;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="MethodDeclarationSyntaxExtensions"/> extensions class.
    /// </summary>
    public static class MethodDeclarationSyntaxExtensions
    {
        #region Methods

        /// <summary>
        /// Adds a comment to a method
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pComment"></param>
        /// <param name="pParams"></param>
        /// <returns></returns>
        public static MethodDeclarationSyntax AddComment(this MethodDeclarationSyntax pThis, string pComment = "TO DO: Comment missing", IEnumerable<Parameter> pParams = null)
        {
            string lMethodName = pThis.Identifier.Text;

            List<XmlNodeSyntax> lXmlNodes = new List<XmlNodeSyntax>()
            {
                SyntaxNodeHelpers.CreateMethodSummary( pComment ),
                SyntaxNodeHelpers.NewXmlLine()
            };

            if ( pParams != null )
            {
                foreach ( Parameter lParam in pParams )
                {
                    lXmlNodes.AddRange( new XmlNodeSyntax[] 
                    {
                         SyntaxNodeHelpers.CreateParamComment( lParam.Name, "The " + lParam.Name ),
                         SyntaxNodeHelpers.NewXmlLine()
                    });
                }
            }

            SyntaxTriviaList lLeadingTrivia = pThis.GetLeadingTrivia().Insert( 0,
                                SyntaxFactory.Trivia(
                                    SyntaxFactory.DocumentationCommentTrivia( SyntaxKind.SingleLineDocumentationCommentTrivia,
                                        SyntaxFactory.List( lXmlNodes ))));

            return pThis.WithLeadingTrivia( lLeadingTrivia );
        }

        #endregion Methods
    }
}
