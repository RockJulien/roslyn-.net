﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;

namespace XRoslyn.Extensions
{
    /// <summary>
    /// Definition of the <see cref="EnumDeclarationSyntaxExtensions"/> extensions class.
    /// </summary>
    public static class EnumDeclarationSyntaxExtensions
    {
        #region Methods

        /// <summary>
        /// Adds a field to a struct
        /// </summary>
        /// <param name="pThis"></param>
        /// <param name="pName"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static EnumDeclarationSyntax AddField(this EnumDeclarationSyntax pThis, string pName, SyntaxGenerator pGenerator)
        {
            EnumMemberDeclarationSyntax lNewField = pGenerator.EnumMember( pName ) as EnumMemberDeclarationSyntax; // Accessor private

            return pThis.AddMembers( lNewField.AddComment() );
        }

        #endregion Methods
    }
}
