﻿namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="Parameter"/> class.
    /// </summary>
    public class Parameter
    {
        #region Fields

        /// <summary>
        /// Stores the parameter's name.
        /// </summary>
        private ParameterName mName;

        /// <summary>
        /// Stores the parameter's type.
        /// </summary>
        private ParameterType mType;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the parameter's name.
        /// </summary>
        public ParameterName Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the parameter's type.
        /// </summary>
        public ParameterType Type
        {
            get
            {
                return this.mType;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Parameter"/> class.
        /// </summary>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        public Parameter(ParameterName pName, ParameterType pType)
        {
            this.mName = pName;
            this.mType = pType;
        }

        #endregion Constructor

        #region Methods
        #endregion Methods
    }
}
