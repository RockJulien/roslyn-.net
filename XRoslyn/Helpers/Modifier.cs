﻿using Microsoft.CodeAnalysis.Editing;

namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="Modifier"/> interface.
    /// </summary>
    public enum Modifier
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,

        /// <summary>
        /// Partial
        /// </summary>
        Partial,

        /// <summary>
        /// Abstract
        /// </summary>
        Abstract,

        /// <summary>
        /// Virtual
        /// </summary>
        Virtual,

        /// <summary>
        /// Override
        /// </summary>
        Override,

        /// <summary>
        /// New
        /// </summary>
        New,

        /// <summary>
        /// Sealed
        /// </summary>
        Sealed,

        /// <summary>
        /// Static
        /// </summary>
        Static
    }

    /// <summary>
    /// Definition of the <see cref="ModifierExtensions"/> class.
    /// </summary>
    internal static class ModifierExtensions
    {
        /// <summary>
        /// Turns a Modifier enum into a Roslyn Declaration Modifier
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        internal static DeclarationModifiers ToDeclarartion(this Modifier pValue)
        {
            switch ( pValue )
            {
                case Modifier.Partial:
                    return DeclarationModifiers.None;
                case Modifier.Abstract:
                    return DeclarationModifiers.None;
                case Modifier.Virtual:
                    return DeclarationModifiers.None;
                case Modifier.Override:
                    return DeclarationModifiers.None;
                case Modifier.New:
                    return DeclarationModifiers.None;
                case Modifier.Sealed:
                    return DeclarationModifiers.None;
                case Modifier.Static:
                    return DeclarationModifiers.None;
                default:
                    return DeclarationModifiers.None;
            }
        }
    }
}
