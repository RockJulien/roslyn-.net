﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using System.Collections.Generic;

namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="SyntaxNodeHelpers"/> class.
    /// </summary>
    public static class SyntaxNodeHelpers
    {
        #region Methods

        /// <summary>
        /// Create a class summary.
        /// </summary>
        /// <param name="pName">The class name</param>
        /// <param name="pComment">The comment.</param>
        /// <returns></returns>
        public static XmlElementSyntax CreateClassSummary(string pName, string pComment = "")
        {
            List<SyntaxToken> lSummaryInfo = new List<SyntaxToken>()
            {
                SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList(),
                                              " class",
                                              " class",
                                              SyntaxFactory.TriviaList()),
                SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                              "\n",
                                              "\n",
                                              SyntaxFactory.TriviaList())
            };

            if ( string.IsNullOrEmpty( pComment ) == false )
            {
                lSummaryInfo.AddRange( new[] 
                {
                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("///")),
                                                  "\n",
                                                  "\n",
                                                  SyntaxFactory.TriviaList()),
                    SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("///") ),
                                                  " " + pComment,
                                                  " " + pComment,
                                                  SyntaxFactory.TriviaList()),
                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                                  "\n",
                                                  "\n",
                                                  SyntaxFactory.TriviaList())
                });
            }

            return SyntaxFactory.XmlExampleElement(
                        SyntaxFactory.XmlText().WithTextTokens(
                            SyntaxFactory.TokenList( new []{
                                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                                                    "\n",
                                                                    "\n",
                                                                    SyntaxFactory.TriviaList()),
                                    SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("///")),
                                                                    " Definition of the ",
                                                                    " Definition of the ",
                                                                    SyntaxFactory.TriviaList())})),
                        SyntaxFactory.XmlNullKeywordElement().WithAttributes(
                            SyntaxFactory.SingletonList<XmlAttributeSyntax>(
                                SyntaxFactory.XmlCrefAttribute(
                                    SyntaxFactory.NameMemberCref(
                                        SyntaxFactory.IdentifierName( pName ))))),
                        SyntaxFactory.XmlText().WithTextTokens( SyntaxFactory.TokenList( lSummaryInfo ) ) )
                    .WithStartTag( SyntaxFactory.XmlElementStartTag(
                            SyntaxFactory.XmlName( SyntaxFactory.Identifier("summary")))
                        .WithLessThanToken( SyntaxFactory.Token( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("///")),
                                                                    SyntaxKind.LessThanToken,
                                                                    SyntaxFactory.TriviaList())))
                    .WithEndTag(  SyntaxFactory.XmlElementEndTag(
                            SyntaxFactory.XmlName( SyntaxFactory.Identifier("summary")))
                        .WithLessThanSlashToken( SyntaxFactory.Token( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                                        SyntaxKind.LessThanSlashToken,
                                                                        SyntaxFactory.TriviaList())));
        }

        /// <summary>
        /// Create a constructor summary.
        /// </summary>
        /// <param name="pName">The class name</param>
        /// <param name="pComment">The comment.</param>
        /// <returns></returns>
        public static XmlElementSyntax CreateConstructorSummary(string pName, string pComment = "")
        {
            List<SyntaxToken> lSummaryInfo = new List<SyntaxToken>()
            {
                SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList(),
                                              " class",
                                              " class",
                                              SyntaxFactory.TriviaList()),
                SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                              "\n",
                                              "\n",
                                              SyntaxFactory.TriviaList())
            };

            if ( string.IsNullOrEmpty( pComment ) == false )
            {
                lSummaryInfo.AddRange( new[] 
                {
                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                  "\n",
                                                  "\n",
                                                  SyntaxFactory.TriviaList()),
                    SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///") ),
                                                  " " + pComment,
                                                  " " + pComment,
                                                  SyntaxFactory.TriviaList()),
                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                                  "\n",
                                                  "\n",
                                                  SyntaxFactory.TriviaList())
                });
            }

            return SyntaxFactory.XmlExampleElement(
                        SyntaxFactory.XmlText().WithTextTokens(
                            SyntaxFactory.TokenList( new []{
                                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                                                    "\n",
                                                                    "\n",
                                                                    SyntaxFactory.TriviaList()),
                                    SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                                    " Initializes a new instance of the ",
                                                                    " Initializes a new instance of the ",
                                                                    SyntaxFactory.TriviaList())})),
                        SyntaxFactory.XmlNullKeywordElement().WithAttributes(
                            SyntaxFactory.SingletonList<XmlAttributeSyntax>(
                                SyntaxFactory.XmlCrefAttribute(
                                    SyntaxFactory.NameMemberCref(
                                        SyntaxFactory.IdentifierName( pName ))))),
                        SyntaxFactory.XmlText().WithTextTokens( SyntaxFactory.TokenList( lSummaryInfo ) ) )
                    .WithStartTag( SyntaxFactory.XmlElementStartTag(
                            SyntaxFactory.XmlName( SyntaxFactory.Identifier("summary")))
                        .WithLessThanToken( SyntaxFactory.Token( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("///")),
                                                                    SyntaxKind.LessThanToken,
                                                                    SyntaxFactory.TriviaList())))
                    .WithEndTag(  SyntaxFactory.XmlElementEndTag(
                            SyntaxFactory.XmlName( SyntaxFactory.Identifier("summary")))
                        .WithLessThanSlashToken( SyntaxFactory.Token( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                                        SyntaxKind.LessThanSlashToken,
                                                                        SyntaxFactory.TriviaList())));
        }

        /// <summary>
        /// Creates an enum value summary.
        /// </summary>
        /// <param name="pName"></param>
        /// <returns></returns>
        public static XmlElementSyntax CreateEnumValueSummary(string pName)
        {
            return SyntaxFactory.XmlExampleElement( 
                        SyntaxFactory.SingletonList<XmlNodeSyntax>( 
                            SyntaxFactory.XmlText().WithTextTokens(
                                SyntaxFactory.TokenList( new [] {
                                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                                                  "\n",
                                                                  "\n",
                                                                  SyntaxFactory.TriviaList()),
                                    SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                                  " " + pName,
                                                                  " " + pName,
                                                                  SyntaxFactory.TriviaList()),
                                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                                                  "\n",
                                                                  "\n",
                                                                  SyntaxFactory.TriviaList())}))))
                                                .WithStartTag( SyntaxFactory.XmlElementStartTag(
                                                        SyntaxFactory.XmlName(
                                                            SyntaxFactory.Identifier("summary")))
                                                .WithLessThanToken( SyntaxFactory.Token( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("///")),
                                                                                         SyntaxKind.LessThanToken,
                                                                                         SyntaxFactory.TriviaList())))
                                                .WithEndTag( SyntaxFactory.XmlElementEndTag(
                                                        SyntaxFactory.XmlName(
                                                            SyntaxFactory.Identifier("summary")))
                                                .WithLessThanSlashToken( SyntaxFactory.Token( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                                                              SyntaxKind.LessThanSlashToken,
                                                                                              SyntaxFactory.TriviaList())));
        }

        /// <summary>
        /// Create a field summary.
        /// </summary>
        /// <param name="pName">The class name</param>
        /// <param name="pComment">The comment.</param>
        /// <returns></returns>
        public static XmlElementSyntax CreateFieldSummary(string pName, string pComment = "")
        {
            List<SyntaxToken> lSummaryInfo = new List<SyntaxToken>()
            {
                SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                              "\n",
                                              "\n",
                                              SyntaxFactory.TriviaList()),
                SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                              " Stores the " + pName,
                                              " Stores the " + pName,
                                              SyntaxFactory.TriviaList()),
                SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                              "\n",
                                              "\n",
                                              SyntaxFactory.TriviaList())
            };

            if ( string.IsNullOrEmpty( pComment ) == false )
            {
                lSummaryInfo.AddRange( new[] 
                {
                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                  "\n",
                                                  "\n",
                                                  SyntaxFactory.TriviaList()),
                    SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///") ),
                                                  " " + pComment,
                                                  " " + pComment,
                                                  SyntaxFactory.TriviaList()),
                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                                  "\n",
                                                  "\n",
                                                  SyntaxFactory.TriviaList())
                });
            }

            return SyntaxFactory.XmlExampleElement( SyntaxFactory.SingletonList<XmlNodeSyntax>(
                                                        SyntaxFactory.XmlText().WithTextTokens(SyntaxFactory.TokenList(lSummaryInfo))))
                                                .WithStartTag(SyntaxFactory.XmlElementStartTag(
                            SyntaxFactory.XmlName(SyntaxFactory.Identifier("summary")))
                        .WithLessThanToken(SyntaxFactory.Token(SyntaxFactory.TriviaList(SyntaxFactory.DocumentationCommentExterior("///")),
                                                                    SyntaxKind.LessThanToken,
                                                                    SyntaxFactory.TriviaList())))
                    .WithEndTag(SyntaxFactory.XmlElementEndTag(
                            SyntaxFactory.XmlName(SyntaxFactory.Identifier("summary")))
                        .WithLessThanSlashToken(SyntaxFactory.Token(SyntaxFactory.TriviaList(SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                                        SyntaxKind.LessThanSlashToken,
                                                                        SyntaxFactory.TriviaList())));
        }

        /// <summary>
        /// Create a property summary.
        /// </summary>
        /// <param name="pName">The class name</param>
        /// <param name="pComment">The comment.</param>
        /// <param name="pHasGetter"></param>
        /// <param name="pHasSetter"></param>
        /// <returns></returns>
        public static XmlElementSyntax CreatePropertySummary(string pName, bool pHasGetter, bool pHasSetter, string pComment = "")
        {
            string lPrefix = "";
            if ( pHasGetter && pHasSetter )
            {
                lPrefix = " Gets or sets ";
            }
            else if ( pHasGetter )
            {
                lPrefix = " Gets ";
            }
            else if ( pHasSetter )
            {
                lPrefix = " Sets ";
            }

            List<SyntaxToken> lSummaryInfo = new List<SyntaxToken>()
            {
                SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                              "\n",
                                              "\n",
                                              SyntaxFactory.TriviaList()),
                SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                              lPrefix + "the " + pName,
                                              lPrefix + "the " + pName,
                                              SyntaxFactory.TriviaList()),
                SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                              "\n",
                                              "\n",
                                              SyntaxFactory.TriviaList())
            };

            if ( string.IsNullOrEmpty( pComment ) == false )
            {
                lSummaryInfo.AddRange( new[] 
                {
                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                  "\n",
                                                  "\n",
                                                  SyntaxFactory.TriviaList()),
                    SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///") ),
                                                  " " + pComment,
                                                  " " + pComment,
                                                  SyntaxFactory.TriviaList()),
                    SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                                  "\n",
                                                  "\n",
                                                  SyntaxFactory.TriviaList())
                });
            }

            return SyntaxFactory.XmlExampleElement( SyntaxFactory.SingletonList<XmlNodeSyntax>(
                                                        SyntaxFactory.XmlText().WithTextTokens(SyntaxFactory.TokenList(lSummaryInfo))))
                                                .WithStartTag(SyntaxFactory.XmlElementStartTag(
                            SyntaxFactory.XmlName(SyntaxFactory.Identifier("summary")))
                        .WithLessThanToken(SyntaxFactory.Token(SyntaxFactory.TriviaList(SyntaxFactory.DocumentationCommentExterior("///")),
                                                                    SyntaxKind.LessThanToken,
                                                                    SyntaxFactory.TriviaList())))
                    .WithEndTag(SyntaxFactory.XmlElementEndTag(
                            SyntaxFactory.XmlName(SyntaxFactory.Identifier("summary")))
                        .WithLessThanSlashToken(SyntaxFactory.Token(SyntaxFactory.TriviaList(SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                                        SyntaxKind.LessThanSlashToken,
                                                                        SyntaxFactory.TriviaList())));
        }

        /// <summary>
        /// Create a method summary.
        /// </summary>
        /// <param name="pComment">The comment.</param>
        /// <returns></returns>
        public static XmlElementSyntax CreateMethodSummary(string pComment = "TO DO: Comment")
        {
            List<SyntaxToken> lSummaryInfo = new List<SyntaxToken>()
            {
                SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                              "\n",
                                              "\n",
                                              SyntaxFactory.TriviaList()),
                SyntaxFactory.XmlTextLiteral( SyntaxFactory.TriviaList( SyntaxFactory.DocumentationCommentExterior("    ///")),
                                              " " + pComment,
                                              " " + pComment,
                                              SyntaxFactory.TriviaList()),
                SyntaxFactory.XmlTextNewLine( SyntaxFactory.TriviaList(),
                                              "\n",
                                              "\n",
                                              SyntaxFactory.TriviaList())
            };

            return SyntaxFactory.XmlExampleElement( SyntaxFactory.SingletonList<XmlNodeSyntax>(
                                                        SyntaxFactory.XmlText().WithTextTokens(SyntaxFactory.TokenList(lSummaryInfo))))
                                                .WithStartTag(SyntaxFactory.XmlElementStartTag(
                            SyntaxFactory.XmlName(SyntaxFactory.Identifier("summary")))
                        .WithLessThanToken(SyntaxFactory.Token(SyntaxFactory.TriviaList(SyntaxFactory.DocumentationCommentExterior("///")),
                                                                    SyntaxKind.LessThanToken,
                                                                    SyntaxFactory.TriviaList())))
                    .WithEndTag(SyntaxFactory.XmlElementEndTag(
                            SyntaxFactory.XmlName(SyntaxFactory.Identifier("summary")))
                        .WithLessThanSlashToken(SyntaxFactory.Token(SyntaxFactory.TriviaList(SyntaxFactory.DocumentationCommentExterior("    ///")),
                                                                        SyntaxKind.LessThanSlashToken,
                                                                        SyntaxFactory.TriviaList())));
        }

        /// <summary>
        /// Creates a param roslyn xml element.
        /// </summary>
        /// <param name="pName">The parameter name</param>
        /// <param name="pComment">The comment.</param>
        /// <returns></returns>
        public static XmlElementSyntax CreateParamComment(string pName, string pComment)
        {
            return SyntaxFactory.XmlExampleElement(
                        SyntaxFactory.SingletonList<XmlNodeSyntax>(
                            SyntaxFactory.XmlText().WithTextTokens(
                                SyntaxFactory.TokenList(
                                    SyntaxFactory.XmlTextLiteral(
                                        SyntaxFactory.TriviaList(),
                                        pComment,
                                        pComment,
                                        SyntaxFactory.TriviaList())))))
                    .WithStartTag(
                        SyntaxFactory.XmlElementStartTag(
                            SyntaxFactory.XmlName(
                                SyntaxFactory.Identifier("param")))
                        .WithLessThanToken(
                            SyntaxFactory.Token(
                                SyntaxFactory.TriviaList(
                                    SyntaxFactory.DocumentationCommentExterior("    ///")),
                                SyntaxKind.LessThanToken,
                                SyntaxFactory.TriviaList()))
                        .WithAttributes(
                            SyntaxFactory.SingletonList<XmlAttributeSyntax>(
                                SyntaxFactory.XmlNameAttribute(
                                    SyntaxFactory.XmlName(
                                        SyntaxFactory.Identifier("name")),
                                    SyntaxFactory.Token(SyntaxKind.DoubleQuoteToken),
                                    SyntaxFactory.IdentifierName( "p" + pName),
                                    SyntaxFactory.Token(SyntaxKind.DoubleQuoteToken)))))
                    .WithEndTag( SyntaxFactory.XmlElementEndTag(
                            SyntaxFactory.XmlName(
                                SyntaxFactory.Identifier("param"))));
        }

        /// <summary>
        /// Create a new xml text node to go to a new line.
        /// </summary>
        /// <returns></returns>
        public static XmlTextSyntax NewXmlLine()
        {
            return SyntaxFactory.XmlText().WithTextTokens(
                        SyntaxFactory.TokenList(
                            SyntaxFactory.XmlTextNewLine(
                                SyntaxFactory.TriviaList(),
                                "\n",
                                "\n",
                                SyntaxFactory.TriviaList())));
        }

        /// <summary>
        /// Creates parameters nodes from parameters infos.
        /// </summary>
        /// <param name="pParams"></param>
        /// <param name="pGenerator"></param>
        /// <returns></returns>
        public static IEnumerable<SyntaxNode> CreateParameterNodes(IEnumerable<Parameter> pParams, SyntaxGenerator pGenerator)
        {
            if ( pParams == null )
            {
                return null;
            }

            List<SyntaxNode> lParameters = new List<SyntaxNode>();
            foreach ( Parameter lParam in pParams )
            {
                lParameters.Add( pGenerator.ParameterDeclaration( "p" + lParam.Name, SyntaxFactory.ParseTypeName( lParam.Type ) ) ); 
            }

            return lParameters;
        }

        #endregion Methods
    }
}
