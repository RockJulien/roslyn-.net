﻿using Microsoft.CodeAnalysis;

namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="AccessLevel"/> enumeration.
    /// </summary>
    public enum AccessLevel
    {
        /// <summary>
        /// Private
        /// </summary>
        Private = 0,

        /// <summary>
        /// Protected
        /// </summary>
        Protected,

        /// <summary>
        /// ProtectedInternal
        /// </summary>
        ProtectedInternal,

        /// <summary>
        /// Internal
        /// </summary>
        Internal,

        /// <summary>
        /// Public
        /// </summary>
        Public
    }

    /// <summary>
    /// Definition of the <see cref="AccessLevelExtensions"/> class.
    /// </summary>
    internal static class AccessLevelExtensions
    {
        /// <summary>
        /// Turns a Modifier enum into a Roslyn Accessibility
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        internal static Accessibility ToAccessibility(this AccessLevel pValue)
        {
            switch ( pValue )
            {
                case AccessLevel.Protected:
                    return Accessibility.Protected;
                case AccessLevel.ProtectedInternal:
                    return Accessibility.ProtectedAndInternal;
                case AccessLevel.Internal:
                    return Accessibility.Internal;
                case AccessLevel.Public:
                    return Accessibility.Public;
                default:
                    return Accessibility.Private;
            }
        }
    }
}
