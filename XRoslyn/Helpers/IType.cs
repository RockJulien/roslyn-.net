﻿using System.Collections.Generic;

namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="IType"/> class.
    /// </summary>
    public interface IType
    {
        #region Properties

        /// <summary>
        /// Gets the type name.
        /// </summary>
        string Name
        {
            get;
        }

        /// <summary>
        /// Gets the type kind.
        /// </summary>
        CompilationUnitType Kind
        {
            get;
        }

        /// <summary>
        /// Gets the interface(s) that is(are) in thistype hierarchy.
        /// </summary>
        /// <returns>The set of interface(s)</returns>
        IEnumerable<IType> Interfaces
        {
            get;
        }

        /// <summary>
        /// Gets the classe(s), abstract(s) included, that is(are) in this type hierarchy.
        /// </summary>
        /// <returns>The set of derived classe(s)</returns>
        IEnumerable<IType> Classes
        {
            get;
        }

        /// <summary>
        /// Gets the overall set of types representing this type hierarchy.
        /// </summary>
        /// <returns>The set of type(s)</returns>
        IEnumerable<IType> AllTypes
        {
            get;
        }

        #endregion Properties
    }
}
