﻿namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="CompilationUnitType"/> class.
    /// </summary>
    public enum CompilationUnitType
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// An enum
        /// </summary>
        Enum,

        /// <summary>
        /// An interface
        /// </summary>
        Interface,

        /// <summary>
        /// A struct
        /// </summary>
        Struct,

        /// <summary>
        /// A class.
        /// </summary>
        Class
    }
}
