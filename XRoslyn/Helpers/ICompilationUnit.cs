﻿using System.Collections.Generic;

namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="ICompilationUnit"/> interface.
    /// </summary>
    public interface ICompilationUnit
    {
        #region Properties



        #endregion Properties

        #region Methods
        
        /// <summary>
        /// Adds comments to the head of the compilation unit element declaration
        /// E.g: Class/Enum/Interface/Struct comments.
        /// </summary>
        /// <param name="pComment">The comment to add.</param>
        void AddComment(string pComment);

        /// <summary>
        /// Adds a new field.
        /// </summary>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        void AddField(AccessLevel pAccessibility, string pName, string pType);

        /// <summary>
        ///  Adds an auto property (property with get/set having empty bodies)
        /// </summary>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        void AddAutoProperty(AccessLevel pAccessibility, string pName, string pType);

        /// <summary>
        /// Adds a full property (property with get/set having bodies using backfields)
        /// NOTE: Backfields with follow the naming rule => m + PropertyName
        /// </summary>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        void AddFullProperty(AccessLevel pAccessibility, string pName, string pType);

        /// <summary>
        /// Adds a default constructor.
        /// </summary>
        /// <param name="pAccessibility"></param>
        void AddDefaultConstructor(AccessLevel pAccessibility);

        /// <summary>
        /// Adds a constructor
        /// </summary>
        /// <param name="pAccessibility"></param>
        /// <param name="pParams"></param>
        void AddConstructor(AccessLevel pAccessibility, IEnumerable<Parameter> pParams);

        /// <summary>
        /// Adds a method
        /// </summary>
        /// <param name="pAccessibility"></param>
        /// <param name="pModifier"></param>
        /// <param name="pReturnType"></param>
        /// <param name="pMethodName"></param>
        /// <param name="pParams"></param>
        void AddMethod(AccessLevel pAccessibility, Modifier pModifier, string pReturnType, string pMethodName, IEnumerable<Parameter> pParams);

        #endregion Methods
    }
}
