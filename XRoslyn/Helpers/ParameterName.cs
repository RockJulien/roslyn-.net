﻿namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="ParameterName"/> class.
    /// </summary>
    public sealed class ParameterName
    {
        #region Fields

        /// <summary>
        /// Stores the parameter name.
        /// </summary>
        private string mParameterName;

        #endregion Fields
        
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ParameterName"/> class.
        /// </summary>
        /// <param name="pParameterName">The parameter name.</param>
        public ParameterName(string pParameterName)
        {
            this.mParameterName = pParameterName;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Implicit cast to string.
        /// </summary>
        /// <param name="pName"></param>
        public static implicit operator string(ParameterName pName)
        {
            return pName.mParameterName;
        }

        #endregion Methods
    }
}
