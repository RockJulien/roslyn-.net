﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using XRoslyn.Context;
using XRoslyn.Extensions;

namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="CompilationUnit"/> class.
    /// </summary>
    internal sealed class CompilationUnit : ICompilationUnit
    {
        #region Fields

        /// <summary>
        /// Stores the roslyn context.
        /// </summary>
        private RoslynContext mContext;

        /// <summary>
        /// Stores the unit declaration syntax.
        /// </summary>
        private BaseTypeDeclarationSyntax mUnitSyntax;

        #endregion Fields
        
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the compilation unit.
        /// </summary>
        /// <param name="pClassSyntax"></param>
        /// <param name="pContext">The roslyn context.</param>
        internal CompilationUnit(BaseTypeDeclarationSyntax pClassSyntax, RoslynContext pContext)
        {
            this.mContext = pContext;
            this.mUnitSyntax = pClassSyntax;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the string representation of this compilation unit.
        /// </summary>
        /// <returns></returns>
        private string GetString()
        {
            return this.mUnitSyntax.NormalizeWhitespace().SyntaxTree.GetString();
        }

        /// <summary>
        /// Adds comments to the head of the compilation unit element declaration
        /// E.g: Class/Enum/Interface/Struct comments.
        /// </summary>
        /// <param name="pComment">The comment to add.</param>
        public void AddComment(string pComment)
        {
            if ( this.mUnitSyntax != null )
            {
                this.mUnitSyntax = this.mUnitSyntax.AddComment( pComment );
            }
        }

        /// <summary>
        /// Adds a new field.
        /// </summary>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        public void AddField(AccessLevel pAccessibility, string pName, string pType)
        {
            if ( this.mUnitSyntax != null )
            {
                this.mUnitSyntax = this.mUnitSyntax.AddField( pAccessibility.ToAccessibility(), pName, pType, this.mContext.SyntaxGenerator );
            }
        }

        /// <summary>
        ///  Adds an auto property (property with get/set having empty bodies)
        /// </summary>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        public void AddAutoProperty(AccessLevel pAccessibility, string pName, string pType)
        {
            if ( this.mUnitSyntax != null )
            {
                this.mUnitSyntax = this.mUnitSyntax.AddAutoProperty( pAccessibility.ToAccessibility(), pName, pType, this.mContext.SyntaxGenerator );
            }
        }

        /// <summary>
        /// Adds a full property (property with get/set having bodies using backfields)
        /// NOTE: Backfields with follow the naming rule => m + PropertyName
        /// </summary>
        /// <param name="pAccessibility"></param>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        public void AddFullProperty(AccessLevel pAccessibility, string pName, string pType)
        {
            if ( this.mUnitSyntax != null )
            {
                this.mUnitSyntax = this.mUnitSyntax.AddFullProperty( pAccessibility.ToAccessibility(), pName, pType, this.mContext.SyntaxGenerator );
            }
        }

        /// <summary>
        /// Adds a default constructor.
        /// </summary>
        /// <param name="pAccessibility"></param>
        public void AddDefaultConstructor(AccessLevel pAccessibility)
        {
            if ( this.mUnitSyntax != null )
            {
                this.mUnitSyntax = this.mUnitSyntax.AddDefaultConstructor( pAccessibility.ToAccessibility(), this.mContext.SyntaxGenerator );
            }
        }

        /// <summary>
        /// Adds a constructor
        /// </summary>
        /// <param name="pAccessibility"></param>
        /// <param name="pParams"></param>
        public void AddConstructor(AccessLevel pAccessibility, IEnumerable<Parameter> pParams)
        {
            if ( this.mUnitSyntax != null )
            {
                this.mUnitSyntax = this.mUnitSyntax.AddConstructor( pAccessibility.ToAccessibility(), pParams, this.mContext.SyntaxGenerator );
            }
        }

        /// <summary>
        /// Adds a method
        /// </summary>
        /// <param name="pAccessibility"></param>
        /// <param name="pModifier"></param>
        /// <param name="pReturnType"></param>
        /// <param name="pMethodName"></param>
        /// <param name="pParams"></param>
        public void AddMethod(AccessLevel pAccessibility, Modifier pModifier, string pReturnType, string pMethodName, IEnumerable<Parameter> pParams)
        {
            if ( this.mUnitSyntax != null )
            {
                this.mUnitSyntax = this.mUnitSyntax.AddMethod( pAccessibility.ToAccessibility(), pModifier.ToDeclarartion(), pReturnType, pMethodName, pParams, this.mContext.SyntaxGenerator );
            }
        }

        /// <summary>
        /// Gets the string representation of this compilation unit.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.GetString();
        }

        #endregion Methods
    }
}
