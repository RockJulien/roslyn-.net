﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Editing;
using System.Collections.Generic;
using XRoslyn.Context;

namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="CompilationUnitGenerator"/> class.
    /// </summary>
    internal class CompilationUnitGenerator
    {
        #region Fields

        /// <summary>
        /// Stores the roslyn context.
        /// </summary>
        private RoslynContext mRoslyn;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CompilationUnitGenerator"/> class.
        /// </summary>
        /// <param name="pRoslyn">The rolsyn generator</param>
        public CompilationUnitGenerator(RoslynContext pRoslyn)
        {
            this.mRoslyn = pRoslyn;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates a new compilation unit element such as an enum, interface, struct or class.
        /// </summary>
        /// <param name="pDeclaration">The declaration.</param>
        /// <returns>The overall compilation unit syntax tree, null if anything wrong.</returns>
        public SyntaxTree Create(CompilationUnitDeclaration pDeclaration)
        {
            if ( pDeclaration == null )
            {
                return null;
            }

            SyntaxTree lReturn = null;
            try
            {
                switch (pDeclaration.Type)
                {
                    case CompilationUnitType.Enum:
                        lReturn = this.CreateEnum( pDeclaration ).SyntaxTree;
                        break;
                    case CompilationUnitType.Interface:
                        lReturn = this.CreateInterface( pDeclaration ).SyntaxTree;
                        break;
                    case CompilationUnitType.Struct:
                        lReturn = this.CreateStruct( pDeclaration ).SyntaxTree;
                        break;
                    case CompilationUnitType.Class:
                        lReturn = this.CreateClass( pDeclaration ).SyntaxTree;
                        break;
                }
            }
            catch
            {
                return null;
            }

            return lReturn;
        }

        /// <summary>
        /// Creates an enum.
        /// </summary>
        /// <param name="pDeclaration">The declaration.</param>
        /// <returns>The compilation unit node.</returns>
        private SyntaxNode CreateEnum(CompilationUnitDeclaration pDeclaration)
        {
            SyntaxGenerator lGenerator = this.mRoslyn.SyntaxGenerator;

            // Generate the enum
            SyntaxNode lEnumDescription = lGenerator.EnumDeclaration( pDeclaration.ClassName, 
                                                                      accessibility: Accessibility.Public,
                                                                      modifiers: default(DeclarationModifiers),
                                                                      members: null );

            List<SyntaxNode> lCompilationUnitDescription = new List<SyntaxNode>()
            {
                lGenerator.NamespaceImportDeclaration( "System" ),
                lGenerator.NamespaceImportDeclaration( "System.Linq" ),
                lGenerator.NamespaceImportDeclaration( "System.Xml.Linq" ),
                lGenerator.NamespaceImportDeclaration( "System.ComponentModel" ),
                lGenerator.NamespaceImportDeclaration( "System.Collections.Generic" ),

                lGenerator.NamespaceDeclaration( pDeclaration.Namespace, lEnumDescription )
            };

            // Finalize the enum.
            return lGenerator.CompilationUnit( lCompilationUnitDescription ).NormalizeWhitespace();
        }

        /// <summary>
        /// Creates an interface.
        /// </summary>
        /// <param name="pDeclaration">The declaration.</param>
        /// <returns>The compilation unit node.</returns>
        private SyntaxNode CreateInterface(CompilationUnitDeclaration pDeclaration)
        {
            SyntaxGenerator lGenerator = this.mRoslyn.SyntaxGenerator;

            // Generate the interface
            SyntaxNode lInterfaceDescription = lGenerator.InterfaceDeclaration( pDeclaration.ClassName, 
                                                                                typeParameters: null,
                                                                                accessibility: Accessibility.Public,
                                                                                interfaceTypes: null,
                                                                                members: null );

            List<SyntaxNode> lCompilationUnitDescription = new List<SyntaxNode>()
            {
                lGenerator.NamespaceImportDeclaration( "System" ),
                lGenerator.NamespaceImportDeclaration( "System.Linq" ),
                lGenerator.NamespaceImportDeclaration( "System.Xml.Linq" ),
                lGenerator.NamespaceImportDeclaration( "System.ComponentModel" ),
                lGenerator.NamespaceImportDeclaration( "System.Collections.Generic" ),

                lGenerator.NamespaceDeclaration( pDeclaration.Namespace, lInterfaceDescription )
            };

            // Finalize the interface.
            return lGenerator.CompilationUnit( lCompilationUnitDescription ).NormalizeWhitespace();
        }

        /// <summary>
        /// Creates a struct.
        /// </summary>
        /// <param name="pDeclaration">The declaration.</param>
        /// <returns>The compilation unit node.</returns>
        private SyntaxNode CreateStruct(CompilationUnitDeclaration pDeclaration)
        {
            SyntaxGenerator lGenerator = this.mRoslyn.SyntaxGenerator;

            // Generate the struct
            SyntaxNode lStructDescription = lGenerator.StructDeclaration( pDeclaration.ClassName, 
                                                                          typeParameters: null,
                                                                          accessibility: Accessibility.Public,
                                                                          modifiers: default(DeclarationModifiers),
                                                                          interfaceTypes: null,
                                                                          members: null);

            List<SyntaxNode> lCompilationUnitDescription = new List<SyntaxNode>()
            {
                lGenerator.NamespaceImportDeclaration( "System" ),
                lGenerator.NamespaceImportDeclaration( "System.Linq" ),
                lGenerator.NamespaceImportDeclaration( "System.Xml.Linq" ),
                lGenerator.NamespaceImportDeclaration( "System.ComponentModel" ),
                lGenerator.NamespaceImportDeclaration( "System.Collections.Generic" ),

                lGenerator.NamespaceDeclaration( pDeclaration.Namespace, lStructDescription )
            };

            // Finalize the struct.
            return lGenerator.CompilationUnit( lCompilationUnitDescription ).NormalizeWhitespace();
        }

        /// <summary>
        /// Creates a class.
        /// </summary>
        /// <param name="pDeclaration">The declaration.</param>
        /// <returns>The compilation unit node.</returns>
        private SyntaxNode CreateClass(CompilationUnitDeclaration pDeclaration)
        {
            SyntaxGenerator lGenerator = this.mRoslyn.SyntaxGenerator;

            // Generate the class
            SyntaxNode lClassDescription = lGenerator.ClassDeclaration( pDeclaration.ClassName, 
                                                                        typeParameters: null,
                                                                        accessibility: Accessibility.Public,
                                                                        modifiers: DeclarationModifiers.Partial,
                                                                        baseType: null,
                                                                        interfaceTypes: null,
                                                                        members: null);

            List<SyntaxNode> lCompilationUnitDescription = new List<SyntaxNode>()
            {
                lGenerator.NamespaceImportDeclaration( "System" ),
                lGenerator.NamespaceImportDeclaration( "System.Linq" ),
                lGenerator.NamespaceImportDeclaration( "System.Xml.Linq" ),
                lGenerator.NamespaceImportDeclaration( "System.ComponentModel" ),
                lGenerator.NamespaceImportDeclaration( "System.Collections.Generic" ),

                lGenerator.NamespaceDeclaration( pDeclaration.Namespace, lClassDescription )
            };

            // Finalize the class.
            return lGenerator.CompilationUnit( lCompilationUnitDescription ).NormalizeWhitespace();
        }

        #endregion Methods
    }
}
