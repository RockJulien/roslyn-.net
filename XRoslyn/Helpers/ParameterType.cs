﻿namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="ParameterType"/> class.
    /// </summary>
    public sealed class ParameterType
    {
        #region Fields

        /// <summary>
        /// Stores the parameter type.
        /// </summary>
        private string mParameterType;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ParameterType"/> class.
        /// </summary>
        /// <param name="pParameterType">The parameter type.</param>
        public ParameterType(string pParameterType)
        {
            this.mParameterType = pParameterType;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Implicit cast to string.
        /// </summary>
        /// <param name="pType"></param>
        public static implicit operator string(ParameterType pType)
        {
            return pType.mParameterType;
        }

        #endregion Methods
    }
}
