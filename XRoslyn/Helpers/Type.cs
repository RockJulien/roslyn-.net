﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using XRoslyn.Context;
using XRoslyn.Extensions;

namespace XRoslyn.Helpers
{
    /// <summary>
    /// Definition of the <see cref="Type"/> class.
    /// </summary>
    internal sealed class Type : IType
    {
        #region Fields

        /// <summary>
        /// Stores the roslyn context.
        /// </summary>
        private RoslynContext mContext;

        /// <summary>
        /// Stores the compilation type.
        /// </summary>
        private INamedTypeSymbol mCompilationType;

        /// <summary>
        /// Stores the type syntax.
        /// </summary>
        private BaseTypeDeclarationSyntax mTypeSyntax;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the compilation type.
        /// </summary>
        internal INamedTypeSymbol CompilationType
        {
            get
            {
                return this.mCompilationType;
            }
        }

        /// <summary>
        /// Gets the type syntax.
        /// </summary>
        internal BaseTypeDeclarationSyntax TypeSyntax
        {
            get
            {
                return this.mTypeSyntax;
            }
        }

        /// <summary>
        /// Gets the type name.
        /// </summary>
        public string Name
        {
            get
            {
                if ( this.mTypeSyntax != null )
                {
                    return this.mTypeSyntax.Name();
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the type kind.
        /// </summary>
        public CompilationUnitType Kind
        {
            get
            {
                if ( this.mTypeSyntax is EnumDeclarationSyntax)
                {
                    return CompilationUnitType.Enum;
                }
                else if (this.mTypeSyntax is InterfaceDeclarationSyntax)
                {
                    return CompilationUnitType.Interface;
                }
                else if (this.mTypeSyntax is StructDeclarationSyntax)
                {
                    return CompilationUnitType.Struct;
                }
                else if (this.mTypeSyntax is ClassDeclarationSyntax)
                {
                    return CompilationUnitType.Class;
                }

                return CompilationUnitType.Unknown;
            }
        }

        /// <summary>
        /// Gets the interface(s) that is(are) in thistype hierarchy.
        /// </summary>
        /// <returns>The set of interface(s)</returns>
        public IEnumerable<IType> Interfaces
        {
            get
            {
                return this.mContext.GetInterfaces( this );
            }
        }

        /// <summary>
        /// Gets the classe(s), abstract(s) included, that is(are) in this type hierarchy.
        /// </summary>
        /// <returns>The set of derived classe(s)</returns>
        public IEnumerable<IType> Classes
        {
            get
            {
                return this.mContext.GetClasses( this );
            }
        }

        /// <summary>
        /// Gets the overall set of types representing this type hierarchy.
        /// </summary>
        /// <returns>The set of type(s)</returns>
        public IEnumerable<IType> AllTypes
        {
            get
            {
                return this.mContext.GetAllTypes( this );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Type"/> class.
        /// </summary>
        /// <param name="pType">The compilation symbol type.</param>
        /// <param name="pContext">The roslyn context</param>
        internal Type(INamedTypeSymbol pType, RoslynContext pContext)
        {
            this.mContext = pContext;
            this.mCompilationType = pType;
            this.mTypeSyntax = pType.ToTypeSyntax( pContext.Compilation );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Type"/> class.
        /// </summary>
        /// <param name="pType">The syntax tree type.</param>
        /// <param name="pContext">The roslyn context</param>
        internal Type(BaseTypeDeclarationSyntax pType, RoslynContext pContext)
        {
            this.mContext = pContext;
            this.mTypeSyntax = pType;
            this.mCompilationType = pType.ToTypeSymbol( pContext.Compilation ) as INamedTypeSymbol;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Turns this type into a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if( this.mTypeSyntax != null )
            {
                return this.mTypeSyntax.ToFullString();
            }

            return base.ToString();
        }

        #endregion Methods
    }
}
