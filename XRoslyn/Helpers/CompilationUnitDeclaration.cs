﻿namespace XRoslyn.Helpers
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CompilationUnitDeclaration"/> class.
    /// </summary>
    public class CompilationUnitDeclaration
    {
        #region Fields

        /// <summary>
        /// Stores the compilation unit type.
        /// </summary>
        private CompilationUnitType mType;

        /// <summary>
        /// Stores the namespace.
        /// </summary>
        private string mNamespace;

        /// <summary>
        /// Stores the class, struct, enum or interface name.
        /// </summary>
        private string mClassName;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the compilation unit type.
        /// </summary>
        public CompilationUnitType Type
        {
            get
            {
                return this.mType;
            }
        }

        /// <summary>
        /// Gets the namespace.
        /// </summary>
        public string Namespace
        {
            get
            {
                return this.mNamespace;
            }
        }

        /// <summary>
        /// Gets the class, struct, enum or interface name.
        /// </summary>
        public string ClassName
        {
            get
            {
                return this.mClassName;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CompilationUnitDeclaration"/> class.
        /// </summary>
        /// <param name="pNamespace">The namespace.</param>
        /// <param name="pClassName">The class name.</param>
        /// <param name="pType">The compilation unit type.</param>
        public CompilationUnitDeclaration(string pNamespace, string pClassName, CompilationUnitType pType)
        {
            this.mNamespace = pNamespace;
            this.mClassName = pClassName;
            this.mType = pType;
        }

        #endregion Constructor
    }
}
