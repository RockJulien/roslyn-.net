﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;
using XRoslyn.Extensions;

namespace XRoslyn.Context
{
    /// <summary>
    /// Definition of the <see cref="BuildContext"/> class.
    /// </summary>
    public sealed class BuildContext : IContext
    {
        #region Fields

        /// <summary>
        /// The compilation default namespaces.
        /// </summary>
        private static readonly IEnumerable<string> DefaultNamespaces = new[]
        {
            "System",
            "System.IO",
            "System.Net",
            "System.Linq",
            "System.Text",
            "System.Text.RegularExpressions",
            "System.Collections.Generic"
        };

        /// <summary>
        /// The project default references.
        /// </summary>
        private static readonly IEnumerable<MetadataReference> DefaultReferences = new[]
        {
            MetadataReference.CreateFromFile( typeof(object).Assembly.Location ),
            MetadataReference.CreateFromFile( typeof(BrowsableAttribute).Assembly.Location ),
            MetadataReference.CreateFromFile( typeof(XElement).Assembly.Location ),
        };

        /// <summary>
        /// Stores the Build workspace.
        /// </summary>
        private MSBuildWorkspace mWork;

        /// <summary>
        /// Stores the roslyn context.
        /// </summary>
        private RoslynContext mRoslyn;

        /// <summary>
        /// Stores the solution involved in the build context. 
        /// NOTE: Can be Null.
        /// </summary>
        private Solution mSolution;

        /// <summary>
        /// Stores the project involved in the build context. 
        /// NOTE: Cannot be Null as at least a project must be the build target.
        /// </summary>
        private Project mProject;

        /// <summary>
        /// Stores the set of namespaces for the projects.
        /// </summary>
        private HashSet<string> mNamespaces;

        /// <summary>
        /// Stores the set of references for the project.
        /// </summary>
        private HashSet<MetadataReference> mReferences;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the build context workspace.
        /// </summary>
        internal Workspace Work
        {
            get
            {
                return this.mWork;
            }
        }

        /// <summary>
        /// Gets the roslyn context
        /// </summary>
        public RoslynContext Roslyn
        {
            get
            {
                if ( this.mRoslyn == null )
                {
                    this.CreateRoslynContext();
                }

                return this.mRoslyn;
            }
        }

        /// <summary>
        /// Gets the solution involved in the build context. 
        /// NOTE: Can be Null.
        /// </summary>
        public Solution Solution
        {
            get
            {
                return this.mSolution;
            }
        }

        /// <summary>
        /// Gets the project involved in the build context. 
        /// NOTE: Cannot be Null as at least a project must be the build target.
        /// </summary>
        public Project Project
        {
            get
            {
                return this.mProject;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BuildContext"/> class.
        /// </summary>
        /// <param name="pProjectPath"></param>
        /// <param name="pSolutionPath"></param>
        /// <param name="pReferences"></param>
        /// <param name="pNamespaces"></param>
        public BuildContext(string pProjectPath, string pSolutionPath = null, IEnumerable<string> pReferences = null, IEnumerable<string> pNamespaces = null)
        {
            this.mWork = MSBuildWorkspace.Create();
            this.mWork.LoadMetadataForReferencedProjects = true;

            // Creates references and namespaces caches.
            if ( pReferences == null ||
                 pReferences.Any() == false )
            {
                this.mReferences = new HashSet<MetadataReference>( DefaultReferences );
            }
            else
            {
                try
                {
                    this.mReferences = new HashSet<MetadataReference>();
                    foreach ( string lReferenceLocation in pReferences )
                    {
                        this.mReferences.Add( MetadataReference.CreateFromFile( lReferenceLocation ) );
                    }
                }
                catch
                {
                    Console.WriteLine( "Bas reference location. Please check so that locations be valid." );
                    this.mReferences = new HashSet<MetadataReference>( DefaultReferences );
                }
            }

            if ( pNamespaces == null ||
                 pNamespaces.Any() == false )
            {
                this.mNamespaces = new HashSet<string>( DefaultNamespaces );
            }
            else
            {
                this.mNamespaces = new HashSet<string>( pNamespaces );
            }

            if ( string.IsNullOrEmpty( pSolutionPath ) )
            {
                // Load the project straight.
                this.mProject = this.mWork.OpenProjectAsync( pProjectPath ).Result;
                if ( this.mProject == null )
                {
                    Console.WriteLine( "Project not loaded" );
                    return;
                }

                this.ReviewWorkspaceError();

                this.mProject = this.mProject.AddMetadataReferences( this.mReferences );
            }
            else
            {
                // Load the solution.
                this.mSolution = this.mWork.OpenSolutionAsync( pSolutionPath ).Result;
                if ( this.mSolution == null )
                {
                    Console.WriteLine( "Solution not loaded" );
                    return;
                }

                this.ReviewWorkspaceError();

                // Get the project in it.
                string lProjectName = pProjectPath.Split( '/' ).Last().Replace( ".csproj", "" );
                this.mProject = this.mSolution.Projects.FirstOrDefault( pElt => pElt.Name == lProjectName );
                if ( this.mProject == null )
                {
                    Console.WriteLine( "Project not loaded" );
                    return;
                }

                this.ReviewWorkspaceError();

                // Ensures there are minimal references.
                if ( this.mProject.MetadataReferences.Count == 0 )
                {
                    this.mProject = this.mProject.AddMetadataReferences( this.mReferences );
                }
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Reviews the workspace error(s) if any.
        /// </summary>
        /// <returns></returns>
        public bool ReviewWorkspaceError()
        {
            return this.mWork.ReviewWorkspaceError();
        }

        /// <summary>
        /// Creates the roslyn context.
        /// </summary>
        private void CreateRoslynContext()
        {
            this.mRoslyn = new RoslynContext( this, this.mNamespaces );
        }

        #endregion Methods
    }
}
