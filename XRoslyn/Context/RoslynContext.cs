﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using XRoslyn.Extensions;
using XRoslyn.Helpers;

namespace XRoslyn.Context
{
    /// <summary>
    /// Definition of the <see cref="RoslynContext"/> class.
    /// </summary>
    public sealed class RoslynContext : IContext
    {
        #region Fields

        /// <summary>
        /// Stores the constant empty list of base types.
        /// </summary>
        private static readonly IEnumerable<IType> sEmptyList = new List<IType>();

        /// <summary>
        /// Stores the build context.
        /// </summary>
        private BuildContext mBuildContext;

        /// <summary>
        /// Stores the compilation unit (class/interface/struct/enum) generator.
        /// </summary>
        private CompilationUnitGenerator mCompilationUnitGenerator;

        /// <summary>
        /// Stores the roslyn workspace compilation if any. Can be Null (e.g: creation of code from scratch)
        /// </summary>
        private Compilation mCompilation;

        /// <summary>
        /// Stores the compilation diagnostic info.
        /// </summary>
        private ImmutableArray<Diagnostic> mCompilationInfo;

        /// <summary>
        /// Stores the roslyn code generator.
        /// </summary>
        private SyntaxGenerator mGenerator;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the compilation unit (class/interface/struct/enum) generator.
        /// </summary>
        internal CompilationUnitGenerator CompilationUnitGenerator
        {
            get
            {
                if ( this.mCompilationUnitGenerator == null )
                {
                    this.mCompilationUnitGenerator = new CompilationUnitGenerator( this );
                }

                return this.mCompilationUnitGenerator;
            }
        }

        /// <summary>
        /// Gets the build context.
        /// </summary>
        public BuildContext BuildContext
        {
            get
            {
                return this.mBuildContext;
            }
        }
        
        /// <summary>
        /// Gets the flag indicating whether the contextual compilation has any errors or not.
        /// </summary>
        public bool HasError
        {
            get
            {
                if ( this.mCompilation != null &&
                     this.mCompilationInfo == null )
                {
                    this.mCompilationInfo = this.mCompilation.GetDiagnostics();
                }

                if ( this.mCompilationInfo != null )
                {
                    return this.mCompilationInfo.Any( pElt => pElt.Severity == DiagnosticSeverity.Error );
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the roslyn workspace compilation if any. Can be Null (e.g: creation of code from scratch)
        /// </summary>
        internal Compilation Compilation
        {
            get
            {
                return this.mCompilation;
            }
        }

        /// <summary>
        /// Gets the roslyn syntax generator.
        /// </summary>
        internal SyntaxGenerator SyntaxGenerator
        {
            get
            {
                return this.mGenerator;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RoslynContext"/> class.
        /// </summary>
        /// <param name="pBuildContext">The build context.</param>
        /// <param name="pNamespaces">The namespaces usings.</param>
        internal RoslynContext(BuildContext pBuildContext, IEnumerable<string> pNamespaces)
        {
            this.mBuildContext = pBuildContext;
            if ( this.mBuildContext == null )
            {
                // Assure a minimal behavior
                this.mBuildContext = new BuildContext( null );
            }
            
            if ( this.mBuildContext.Project != null )
            {
                this.mGenerator = SyntaxGenerator.GetGenerator( this.mBuildContext.Project );

                CSharpCompilationOptions lOptions = this.mBuildContext.Project.CompilationOptions as CSharpCompilationOptions;
                lOptions = lOptions.WithUsings( pNamespaces )
                                   .WithReportSuppressedDiagnostics( false );
                this.mCompilation = this.mBuildContext.Project.GetCompilationAsync().Result;
            }
            else
            {
                this.mGenerator = SyntaxGenerator.GetGenerator( this.mBuildContext.Work, LanguageNames.CSharp );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Writes to the console the symbol table of the current contextual compilation if any.
        /// </summary>
        /// <returns>True if any contextual compilation, false otherwise.</returns>
        public bool ReviewSymbolTable()
        {
            if ( this.mCompilation != null )
            {
                this.mCompilation.ReviewSymbolTable();
                return true;
            }
            else
            {
                Console.WriteLine( "No symbol table." );
            }

            return false;
        }

        /// <summary>
        /// Writes to the console the contextual compilation error(s) if any.
        /// </summary>
        /// <returns>True if any errors, false otherwise.</returns>
        public bool ReviewCompilationErrors()
        {
            if ( this.mCompilation != null )
            {
                if ( this.mCompilationInfo == null )
                {
                    this.mCompilationInfo = this.mCompilation.GetDiagnostics();
                }
            }

            if ( this.mCompilationInfo != null )
            {
                bool lResult = false;
                foreach ( Diagnostic lDiagnostic in this.mCompilationInfo.Where( pDiagnostic => pDiagnostic.Severity == DiagnosticSeverity.Error ) )
                {
                    lResult = true;
                    Console.WriteLine( lDiagnostic.GetMessage() + lDiagnostic.Location );
                }

                return lResult;
            }

            return false;
        }

        /// <summary>
        /// Creates a new compilation unit
        /// </summary>
        /// <param name="pDeclarartion">The compilation unit declaration</param>
        /// <param name="pUnit">The new compilation unit.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public bool Create(CompilationUnitDeclaration pDeclarartion, out ICompilationUnit pUnit)
        {
            pUnit = null;

            SyntaxTree lTree = this.CompilationUnitGenerator.Create( pDeclarartion );
            if ( lTree != null )
            {
                pUnit = new CompilationUnit( lTree.Class(), this );
                return true;
            }

            return false;
        }

        /// <summary>
        /// Produces the Dll associated with the contextual compilation if any.
        /// </summary>
        /// <param name="pDllName">The dll name.</param>
        /// <returns>True if produced, false otherwise.</returns>
        public bool Produce(string pDllName)
        {
            if ( string.IsNullOrEmpty( pDllName ) ||
                 this.mCompilation == null )
            {
                return false;
            }

            string lDllName = pDllName.Replace( ".dll", "" );
            if ( this.ReviewCompilationErrors() == false )
            {
                this.mCompilation.Emit( string.Format( "{0}.dll", lDllName ), 
                                        string.Format( "{0}.pdb", lDllName ), 
                                        string.Format( "{0}.xml", lDllName ) );
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the given fullname corresponding type.
        /// </summary>
        /// <param name="pFullName"></param>
        /// <returns>The type, null otherwise.</returns>
        public IType GetType(string pFullName)
        {
            if ( this.mCompilation != null )
            {
                return new Helpers.Type( this.mCompilation.GetTypeByMetadataName( pFullName ), this );
            }

            return null;
        }

        /// <summary>
        /// Gets the interface(s) that is(are) in the given type hierarchy.
        /// </summary>
        /// <param name="pBaseType">The base type.</param>
        /// <returns>The set of interface(s)</returns>
        public IEnumerable<IType> GetInterfaces(IType pBaseType)
        {
            Helpers.Type lCast = pBaseType as Helpers.Type;
            if ( this.mCompilation != null )
            {
                foreach ( BaseTypeDeclarationSyntax lInterface in this.mCompilation.FindDerivedInterfaces( lCast.CompilationType ) )
                {
                    yield return new Helpers.Type( lInterface, this );
                }
            }

            yield break;
        }

        /// <summary>
        /// Gets the classe(s), abstract(s) included, that is(are) in the given type hierarchy.
        /// </summary>
        /// <param name="pBaseType">The base type.</param>
        /// <returns>The set of derived classe(s)</returns>
        public IEnumerable<IType> GetClasses(IType pBaseType)
        {
            Helpers.Type lCast = pBaseType as Helpers.Type;
            if ( this.mCompilation != null )
            {
                foreach ( BaseTypeDeclarationSyntax lClass in this.mCompilation.FindDerivedClasses( lCast.CompilationType ) )
                {
                    yield return new Helpers.Type( lClass, this );
                }
            }

            yield break;
        }

        /// <summary>
        /// Gets the overall set of types representing the given type hierarchy.
        /// </summary>
        /// <param name="pBaseType">The base type.</param>
        /// <returns>The set of type(s)</returns>
        public IEnumerable<IType> GetAllTypes(IType pBaseType)
        {
            Helpers.Type lCast = pBaseType as Helpers.Type;
            if ( this.mCompilation != null )
            {
                foreach ( BaseTypeDeclarationSyntax lClass in this.mCompilation.FindAllDerivedByType( lCast.CompilationType ) )
                {
                    yield return new Helpers.Type( lClass, this );
                }
            }

            yield break;
        }

        #endregion Methods
    }
}
