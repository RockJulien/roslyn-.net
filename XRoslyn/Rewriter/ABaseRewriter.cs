﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;
using XRoslyn.Context;
using XRoslyn.Extensions;
using XRoslyn.Helpers;

namespace XRoslyn.Rewriter
{
    /// <summary>
    /// Definition of the <see cref="ABaseRewriter"/> class.
    /// </summary>
    public abstract class ABaseRewriter : CSharpSyntaxRewriter
    {
        #region Fields

        /// <summary>
        /// Stores the new namespace.
        /// </summary>
        private string mNamespace;

        /// <summary>
        /// Stores the new usings.
        /// </summary>
        private List<SyntaxNode> mUsings;

        /// <summary>
        /// Stores the roslyn context.
        /// </summary>
        protected RoslynContext mContext;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Allow to visit everything in depth of not.
        /// </summary>
        public override bool VisitIntoStructuredTrivia
        {
            get
            {
                return true;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ABaseRewriter"/> class.
        /// </summary>
        /// <param name="pNamespace">The new namespace.</param>
        /// <param name="pUsings">The new usings.</param>
        /// <param name="pContext">The Roslyn context.</param>
        protected ABaseRewriter(string pNamespace, IEnumerable<string> pUsings, RoslynContext pContext)
        {
            this.mNamespace = pNamespace;
            this.mUsings = new List<SyntaxNode>();
            if ( pUsings != null )
            {
                foreach ( string lUsing in pUsings )
                {
                    this.mUsings.Add( pContext.SyntaxGenerator.NamespaceImportDeclaration( lUsing ) );
                }
            }

            this.mContext = pContext;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Rewrites the given type.
        /// </summary>
        /// <param name="pType"></param>
        /// <returns></returns>
        public ICompilationUnit Rewrite(IType pType)
        {
            string lTypeName = pType.Name;
            Helpers.Type lCast = pType as Helpers.Type;

            BaseTypeDeclarationSyntax lRewroteType = this.Visit( lCast.TypeSyntax ) as BaseTypeDeclarationSyntax;

            List<SyntaxNode> lCompilationUnitDescription = new List<SyntaxNode>();
            lCompilationUnitDescription.AddRange( this.mUsings );
            lCompilationUnitDescription.Add( this.mContext.SyntaxGenerator.NamespaceDeclaration( this.mNamespace, lRewroteType ) );

            CompilationUnitSyntax lNewUnit = this.mContext.SyntaxGenerator.CompilationUnit( lCompilationUnitDescription ).NormalizeWhitespace() as CompilationUnitSyntax;
            BaseTypeDeclarationSyntax lNewType = lNewUnit.DescendantNodes().OfType<BaseTypeDeclarationSyntax>().FirstOrDefault( pElt => pElt.Name() == lTypeName );

            return new CompilationUnit( lNewType, this.mContext );
        }

        /// <summary>
        /// Visit a start region syntax node.
        /// </summary>
        /// <param name="pNode"></param>
        /// <returns></returns>
        public override SyntaxNode VisitRegionDirectiveTrivia(RegionDirectiveTriviaSyntax pNode)
        {
            return base.VisitRegionDirectiveTrivia( pNode );
        }

        /// <summary>
        /// Visit an end region syntax node.
        /// </summary>
        /// <param name="pNode"></param>
        /// <returns></returns>
        public override SyntaxNode VisitEndRegionDirectiveTrivia(EndRegionDirectiveTriviaSyntax pNode)
        {
            return base.VisitEndRegionDirectiveTrivia( pNode );
        }

        #endregion Methods
    }
}
