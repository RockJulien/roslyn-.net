﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;
using XRoslyn.Context;

namespace XRoslyn.Rewriter
{
    /// <summary>
    /// Definition of the <see cref="ClassRewriter"/> class.
    /// </summary>
    public class ClassRewriter : ABaseRewriter
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassRewriter"/> class.
        /// </summary>
        /// <param name="pNamespace">The namespace</param>
        /// <param name="pUsings">The usings</param>
        /// <param name="pContext">The Roslyn context.</param>
        public ClassRewriter(string pNamespace, IEnumerable<string> pUsings, RoslynContext pContext) :
        base( pNamespace, pUsings, pContext )
        {
            
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Visit the class node.
        /// </summary>
        /// <param name="pNode"></param>
        /// <returns></returns>
        public override SyntaxNode VisitClassDeclaration(ClassDeclarationSyntax pNode)
        {
            return base.VisitClassDeclaration( pNode );
        }

        /// <summary>
        /// Visit a property node
        /// </summary>
        /// <param name="pNode"></param>
        /// <returns></returns>
        public override SyntaxNode VisitPropertyDeclaration(PropertyDeclarationSyntax pNode)
        {
            // If abstract class, do not modify
            bool lCanGenerateProperty = true;
            if ( pNode.Modifiers.Any( Microsoft.CodeAnalysis.CSharp.SyntaxKind.AbstractKeyword ) == false ) // If not abstract
            {
                // Modify property so that the getter ans setter bodies uses the underlying object.

            }

            if ( lCanGenerateProperty )
            {
                return base.VisitPropertyDeclaration( pNode );
            }

            return null;
        }

        #endregion Methods
    }
}
