﻿using System.Collections.Generic;
using XRoslyn.Context;

namespace XRoslyn.Rewriter
{
    /// <summary>
    /// Definition of the <see cref="EnumRewriter"/> class.
    /// </summary>
    public class EnumRewriter : ABaseRewriter
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumRewriter"/> class.
        /// </summary>
        /// <param name="pNamespace">The namespace</param>
        /// <param name="pUsings">The usings</param>
        /// <param name="pContext">The Roslyn context.</param>
        public EnumRewriter(string pNamespace, IEnumerable<string> pUsings, RoslynContext pContext) :
        base( pNamespace, pUsings, pContext )
        {

        }

        #endregion Constructor
    }
}
