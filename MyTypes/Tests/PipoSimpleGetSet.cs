﻿using System;

namespace MyTypes.Tests
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PipoSimpleGetSet"/> class.
    /// </summary>
    public class PipoSimpleGetSet : APipo
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public override string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo count.
        /// </summary>
        public override int PipoCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo context.
        /// </summary>
        public override object Context
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipoSimpleGetSet"/> class.
        /// </summary>
        public PipoSimpleGetSet() :
        this( string.Empty, 1, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipoSimpleGetSet"/> class.
        /// </summary>
        /// <param name="pName">The pipo name</param>
        /// <param name="pCount">The pipo count</param>
        /// <param name="pContext">The pipo context</param>
        public PipoSimpleGetSet(string pName, int pCount, object pContext)
        {
            this.Name = pName;
            this.PipoCount = pCount;
            this.Context = pContext;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        public override void PipoMethodNoParameter()
        {
            Console.WriteLine( "PipoMethodNoParameter" );
        }

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        /// <param name="pPipoString"></param>
        /// <param name="pContext"></param>
        public override void PipoMethodWithParameter(string pPipoString, object pContext)
        {
            Console.WriteLine( string.Format( "PipoMethodWithParameter : {0} ({1})", pPipoString, pContext ) );
        }

        #endregion Methods
    }
}
