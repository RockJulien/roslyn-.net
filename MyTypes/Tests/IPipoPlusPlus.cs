﻿namespace MyTypes.Tests
{
    /// <summary>
    /// Definition of the <see cref="IPipoPlusPlus"/> interface
    /// </summary>
    public interface IPipoPlusPlus : IPipo
    {
    }
}
