﻿namespace MyTypes.Tests
{
    /// <summary>
    /// Definitionof the <see cref="IToGenerate"/> class.
    /// 
    /// Test to have multiple interface but not al to generate back.
    /// </summary>
    public interface IToGenerate
    {

    }
}
