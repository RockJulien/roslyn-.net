﻿namespace MyTypes.Tests
{
    /// <summary>
    /// Definition of the <see cref="PipoEnum"/> enumeration.
    /// </summary>
    public enum PipoEnum
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,

        /// <summary>
        /// Coconuts
        /// </summary>
        Coconuts,

        /// <summary>
        /// Marsipulami
        /// </summary>
        Marsipulami,

        /// <summary>
        /// Doudou
        /// </summary>
        Doudou,

        /// <summary>
        /// Shoubaka
        /// </summary>
        Shoubaka
    }
}
