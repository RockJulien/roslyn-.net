﻿using System.ComponentModel;

namespace MyTypes.Tests
{
    /// <summary>
    /// Definition of the <see cref="IPipo"/> interface.
    /// </summary>
    public interface IPipo : IToGenerate
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo count.
        /// </summary>
        int PipoCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo context.
        /// </summary>
        object Context
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        void PipoMethodNoParameter();

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        /// <param name="pPipoString"></param>
        /// <param name="pContext"></param>
        [Runtime]
        void PipoMethodWithParameter(string pPipoString, object pContext);

        #endregion Methods
    }
}
