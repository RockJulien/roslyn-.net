﻿namespace MyTypes.Tests
{
    /// <summary>
    /// Definition of the <see cref="APipo"/> class.
    /// </summary>
    public abstract class APipo : IPipo
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public abstract string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo count.
        /// </summary>
        public abstract int PipoCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo context.
        /// </summary>
        public abstract object Context
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        public abstract void PipoMethodNoParameter();

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        /// <param name="pPipoString"></param>
        /// <param name="pContext"></param>
        public abstract void PipoMethodWithParameter(string pPipoString, object pContext);

        #endregion Methods
    }
}
