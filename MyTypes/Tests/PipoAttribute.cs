﻿using System;

namespace MyTypes.Tests
{
    /// <summary>
    /// Definition of the <see cref="PipoAttribute"/> class.
    /// </summary>
    public class PipoAttribute : Attribute
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipoAttribute"/> class.
        /// </summary>
        public PipoAttribute()
        {

        }

        #endregion Constructor
    }
}
