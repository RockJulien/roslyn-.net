﻿using System;

namespace MyTypes.Tests
{
    /// <summary>
    /// Definition of the <see cref="RuntimeAttribute"/> class.
    /// </summary>
    public class RuntimeAttribute : Attribute
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RuntimeAttribute"/> class.
        /// </summary>
        public RuntimeAttribute()
        {

        }

        #endregion Constructor
    }
}
