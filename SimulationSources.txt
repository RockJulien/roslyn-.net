    /// <summary>
    /// Definition of the <see cref="APipo"/> class.
    /// </summary>
    public abstract class APipo : IPipo
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public abstract string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo count.
        /// </summary>
        public abstract int PipoCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo context.
        /// </summary>
        public abstract object Context
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        public abstract void PipoMethodNoParameter();

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        /// <param name="pPipoString"></param>
        /// <param name="pContext"></param>
        public abstract void PipoMethodWithParameter(string pPipoString, object pContext);

        #endregion Methods
    }


    /// <summary>
    /// Definition of the <see cref="IPipo"/> interface.
    /// </summary>
    public interface IPipo : IToGenerate
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo count.
        /// </summary>
        int PipoCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo context.
        /// </summary>
        object Context
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        void PipoMethodNoParameter();

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        /// <param name="pPipoString"></param>
        /// <param name="pContext"></param>
        [Runtime]
        void PipoMethodWithParameter(string pPipoString, object pContext);

        #endregion Methods
    }


    /// <summary>
    /// Definition of the <see cref="IPipoPlusPlus"/> interface
    /// </summary>
    public interface IPipoPlusPlus : IPipo
    {
    }


    /// <summary>
    /// Initializes a new instance of the <see cref="PipoMemberedGetSet"/> class.
    /// </summary>
    public class PipoMemberedGetSet : APipo
    {
        #region Fields

        /// <summary>
        /// Stores the name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the pipo count.
        /// </summary>
        private int mPipoCount;

        /// <summary>
        /// Stores the pipo context.
        /// </summary>
        private object mContext;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public override string Name
        {
            get
            {
                return this.mName;
            }
            set
            {
                this.mName = value;
            }
        }

        /// <summary>
        /// Gets or sets the pipo count.
        /// </summary>
        public override int PipoCount
        {
            get
            {
                return this.mPipoCount;
            }
            set
            {
                this.mPipoCount = value;
            }
        }

        /// <summary>
        /// Gets or sets the pipo context.
        /// </summary>
        public override object Context
        {
            get
            {
                return this.mContext;
            }
            set
            {
                this.mContext = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipoMemberedGetSet"/> class.
        /// </summary>
        public PipoMemberedGetSet() :
        this( string.Empty, 1, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipoMemberedGetSet"/> class.
        /// </summary>
        /// <param name="pName">The pipo name</param>
        /// <param name="pCount">The pipo count</param>
        /// <param name="pContext">The pipo context</param>
        public PipoMemberedGetSet(string pName, int pCount, object pContext)
        {
            this.mName = pName;
            this.mPipoCount = pCount;
            this.mContext = pContext;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        public override void PipoMethodNoParameter()
        {
            Console.WriteLine( "PipoMethodNoParameter" );
        }

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        /// <param name="pPipoString"></param>
        /// <param name="pContext"></param>
        public override void PipoMethodWithParameter(string pPipoString, object pContext)
        {
            Console.WriteLine( string.Format( "PipoMethodWithParameter : {0} ({1})", pPipoString, pContext ) );
        }

        #endregion Methods
    }


    /// <summary>
    /// Initializes a new instance of the <see cref="PipoSimpleGetSet"/> class.
    /// </summary>
    public class PipoSimpleGetSet : APipo
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public override string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo count.
        /// </summary>
        public override int PipoCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the pipo context.
        /// </summary>
        public override object Context
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipoSimpleGetSet"/> class.
        /// </summary>
        public PipoSimpleGetSet() :
        this( string.Empty, 1, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipoSimpleGetSet"/> class.
        /// </summary>
        /// <param name="pName">The pipo name</param>
        /// <param name="pCount">The pipo count</param>
        /// <param name="pContext">The pipo context</param>
        public PipoSimpleGetSet(string pName, int pCount, object pContext)
        {
            this.Name = pName;
            this.PipoCount = pCount;
            this.Context = pContext;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        public override void PipoMethodNoParameter()
        {
            Console.WriteLine( "PipoMethodNoParameter" );
        }

        /// <summary>
        /// Pipo method without parameters.
        /// </summary>
        /// <param name="pPipoString"></param>
        /// <param name="pContext"></param>
        public override void PipoMethodWithParameter(string pPipoString, object pContext)
        {
            Console.WriteLine( string.Format( "PipoMethodWithParameter : {0} ({1})", pPipoString, pContext ) );
        }

        #endregion Methods
    }


