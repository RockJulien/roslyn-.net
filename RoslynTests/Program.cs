﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using XRoslyn.Context;
using XRoslyn.Helpers;
using XRoslyn.Rewriter;
using XRoslyn.Visitor;

namespace RoslynTests
{
    /// <summary>
    /// Definition of the <see cref="Program"/>
    /// </summary>
    class Program
    {
        /// <summary>
        /// Stores the constant generated namespace.
        /// </summary>
        private const string cGeneratedNamespace = "GeneratedNamespacePipo";

        /// <summary>
        /// Stores the constant generated usings.
        /// </summary>
        private readonly static List<string> cGeneratedUsings = new List<string>()
        {
            "System",
            "System.Linq",
            "System.Xml.Linq",
            "System.ComponentModel",
            "System.Collections.Generic"
        };

        /// <summary>
        /// Main entry point.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // Turns code to SyntaxTree
            var lVisitor = new SimpleVisitor("a=b+c;");
            lVisitor.Visit();

            // Turns the SyntaxTree back to code.
            Console.WriteLine();
            Console.WriteLine("We can get back to the original code by call ToFullString ");
            Console.WriteLine( lVisitor.GetString() );
            Console.WriteLine();

            // Création du build context.
            string lWorkingDirectory = Environment.CurrentDirectory;
            string lProjectFilePath = string.Format( @"{0}\{1}", lWorkingDirectory, @"..\..\MyTypes\MyTypes.csproj" );
            BuildContext lBuild = new BuildContext( lProjectFilePath );
            
            // Récupération du Roslyn context.
            RoslynContext lContext = lBuild.Roslyn;

            lContext.ReviewSymbolTable();

            Console.WriteLine();
            //Console.ReadLine();

            // Find derived types.
            IType lTargetType = lContext.GetType( "MyTypes.Tests.IToGenerate" );
            if ( lTargetType == null )
            {
                Console.WriteLine("-- ERROR: IToGenerate type not found.");
                return;
            }

            Console.WriteLine();
            Console.WriteLine("-- All derived interfaces of IToGenerate");
            IEnumerable<IType> lDerivedTypes = lContext.GetInterfaces( lTargetType );
            foreach (IType lDerivedType in lDerivedTypes)
            {
                Console.WriteLine("   * " + lDerivedType.Name);
            }

            Console.WriteLine();
            Console.WriteLine("-- All derived classes of IToGenerate");
            IEnumerable<IType> lDerivedClassesTypes = lContext.GetClasses( lTargetType );
            foreach (IType lDerivedType in lDerivedClassesTypes)
            {
                Console.WriteLine("   * " + lDerivedType.Name);
            }

            Console.WriteLine();
            Console.WriteLine("-- All derived types of IToGenerate");
            IEnumerable<IType> lAllDerivedTypes = lContext.GetAllTypes( lTargetType );
            foreach (IType lDerivedType in lAllDerivedTypes)
            {
                Console.WriteLine("   * " + lDerivedType.Name);
            }

            Console.WriteLine();
            //Console.ReadLine();

            // Emit the Il code from the compilation and creates dll and pdb files.
            if ( lContext.Produce( "MyTypes" ) )
            {
                Console.WriteLine("MyTypes.dll produced.");
            }

            Console.WriteLine();
            //Console.ReadLine();

            // Generate the code back by aiming members tagged with RuntimeAttribute
            //Project lTargetProject = lWork.OpenProjectAsync( @"..\..\..\RoslynTests_4_6_1\RoslynTests_4_6_1.csproj" ).Result;

            // Generate from scratch
            CompilationUnitDeclaration lDeclaration = new CompilationUnitDeclaration("FromScratch", "MyClass", CompilationUnitType.Class);
            ICompilationUnit lNewClass;
            if ( lContext.Create( lDeclaration, out lNewClass ) )
            {
                lNewClass.AddComment("The Big Class");

                lNewClass.AddField(AccessLevel.Private, "Data", "Coconut");

                lNewClass.AddAutoProperty(AccessLevel.Public, "Name", "string");
                lNewClass.AddFullProperty(AccessLevel.Public, "Data", "Coconut");

                lNewClass.AddDefaultConstructor(AccessLevel.Protected );
                lNewClass.AddConstructor(AccessLevel.Public, new List<Parameter>() { new Parameter( new ParameterName( "Name" ), new ParameterType( "MyTypes.Tests.IToGenerate" ) ) } );

                lNewClass.AddMethod(AccessLevel.Public, Modifier.Virtual, "void", "TheBigMethod", new List<Parameter>() { new Parameter( new ParameterName( "Plop" ), new ParameterType( "int" ) ) } );
            }

            Console.WriteLine( lNewClass.ToString() );
            Console.WriteLine();
            //Console.ReadLine();

            StringBuilder lSimulationSource = new StringBuilder();
            StringBuilder lApplicationSource = new StringBuilder();
            foreach (IType lType in lAllDerivedTypes)
            {
                string lTypeStr = lType.ToString();
                lSimulationSource.AppendLine(lTypeStr);
                lSimulationSource.AppendLine();

                Console.WriteLine(" ---- Type description on source side (e.g: Simulation side)");
                Console.WriteLine();
                Console.WriteLine(lTypeStr);
                Console.WriteLine();

                Console.WriteLine(" ---- New type description on target side (e.g: Application side)");
                Console.WriteLine();
                ICompilationUnit lTree = Rewrite( lType, lContext );
                if (lTree != null)
                {
                    string lNewTypeStr = lTree.ToString();
                    lApplicationSource.AppendLine(lNewTypeStr);
                    lApplicationSource.AppendLine();

                    Console.WriteLine(lNewTypeStr);
                    Console.WriteLine();
                }
            }

            // Write to files.
            File.WriteAllText(@"..\..\..\SimulationSources.txt", lSimulationSource.ToString());
            File.WriteAllText(@"..\..\..\ApplicationSources.txt", lApplicationSource.ToString());

            Console.WriteLine();
            Console.ReadLine();
        }

        /// <summary>
        /// Generates a type (interface of class)
        /// </summary>
        /// <param name="pType"></param>
        /// <param name="pContext"></param>
        private static ICompilationUnit Rewrite(IType pType, RoslynContext pContext)
        {
            if ( pType.Kind == CompilationUnitType.Enum )
            {
                return RewriteEnum( pType, pContext );
            }
            else if ( pType.Kind == CompilationUnitType.Interface )
            {
                return RewriteInterface( pType, pContext );
            }
            else if ( pType.Kind == CompilationUnitType.Struct )
            {
                return RewriteStruct( pType, pContext );
            }
            else if ( pType.Kind == CompilationUnitType.Class )
            {
                return RewriteClass( pType, pContext );
            }

            return null;
        }

        /// <summary>
        /// Generates an enum.
        /// </summary>
        /// <param name="pType"></param>
        /// <param name="pContext"></param>
        /// <returns></returns>
        private static ICompilationUnit RewriteEnum(IType pType, RoslynContext pContext)
        {
            return new EnumRewriter( cGeneratedNamespace, cGeneratedUsings, pContext ).Rewrite( pType );
        }

        /// <summary>
        /// Generates an intreface.
        /// </summary>
        /// <param name="pType"></param>
        /// <param name="pContext"></param>
        /// <returns></returns>
        private static ICompilationUnit RewriteInterface(IType pType, RoslynContext pContext)
        {
            return new InterfaceRewriter( cGeneratedNamespace, cGeneratedUsings, pContext ).Rewrite( pType );
        }

        /// <summary>
        /// Generates a class.
        /// </summary>
        /// <param name="pType"></param>
        /// <param name="pContext"></param>
        /// <returns></returns>
        private static ICompilationUnit RewriteClass(IType pType, RoslynContext pContext)
        {
            return new ClassRewriter( cGeneratedNamespace, cGeneratedUsings, pContext ).Rewrite( pType );
        }

        /// <summary>
        /// Generates a struct.
        /// </summary>
        /// <param name="pType"></param>
        /// <param name="pContext"></param>
        /// <returns></returns>
        private static ICompilationUnit RewriteStruct(IType pType, RoslynContext pContext)
        {
            return new StructRewriter( cGeneratedNamespace, cGeneratedUsings, pContext ).Rewrite( pType );
        }
    }
}
